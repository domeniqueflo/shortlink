﻿using MainHandler.Domain.Entities;
using MainHandler.Domain.RepositoriesAbstractions;
using System.Text.Json;
using StackExchange.Redis;
using ProjectLibrary.Realizations;

namespace MainHandler.Persistence.Repositories;

public class RedisLinkLinkRepository : ILinkRepository
{
    private readonly IDatabase _database;

    public RedisLinkLinkRepository(IConnectionMultiplexer redis)
    {
        _database = redis.GetDatabase();
    }
    
    /// <summary>
    /// Get link entity by short code
    /// </summary>
    /// <param name="shortCode">Short code we have</param>
    /// <returns></returns>
    public Link GetLinkByShortCode(string shortCode)
    {
        try
        {
            var linkJson = _database.StringGet(shortCode);      

            if (linkJson.IsNullOrEmpty)
                return null!;
            LinkFullDataModel resultLink = JsonSerializer.Deserialize<LinkFullDataModel>(linkJson);

            Link userResultLink = new Link() { Id = resultLink.Id, ShortLink = resultLink.ShortLink, UserId = resultLink.UserId, MobileLink = resultLink.MobileLink,
            WebLink = resultLink.WebLink};

            return userResultLink!;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

        return default;
    }

#if DEBUG
    
    public void Add(Link linkToAdd)
    {
        var linkJson = JsonSerializer.SerializeToUtf8Bytes(linkToAdd);
        _database.StringSet(linkToAdd.ShortLink, linkJson);
    }
    
#endif    
    
}