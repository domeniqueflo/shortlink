﻿using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using MainHandler.Domain.RepositoriesAbstractions;
using MainHandler.Persistence.Repositories;

namespace MainHandler.Persistence;

public static class DependencyInjection
{
    public static IServiceCollection AddPersistence(this IServiceCollection serviceCollection)
    {
        //var redisConnectionString = Environment.GetEnvironmentVariable("redis");
        serviceCollection.AddSingleton<IConnectionMultiplexer>(serviceProvider =>
            ConnectionMultiplexer.Connect("redis:6379"));

        serviceCollection.AddScoped<ILinkRepository, RedisLinkLinkRepository>();
        
        return serviceCollection;
    }
}