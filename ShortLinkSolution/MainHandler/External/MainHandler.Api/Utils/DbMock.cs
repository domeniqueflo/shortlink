using MainHandler.Domain.Entities;
using MainHandler.Domain.RepositoriesAbstractions;

namespace MainHandler.Api.Utils;

public class DbMock : ILinkRepository
{
    public DbMock()
    {
        ListCollection = new List<Link>()
        {
            new() { ShortLink = "ggl", WebLink = "google.com" },
            new() { ShortLink = "yn", WebLink = "yandex.ru" },
        };
    }
    
    private List<Link> ListCollection;

    public Link GetLinkByShortCode(string shortCode)
    {
        var result = ListCollection.FirstOrDefault(x => x.ShortLink == shortCode); 
        
        return result!;
    }
    
#if DEBUG
    
    public void Add(Link linkToAdd) { }
    
#endif    
}