using MainHandler.Application;
using MainHandler.Domain.RepositoriesAbstractions;
using MainHandler.Infrastructure;
using MainHandler.Persistence;
using MainHandler.Persistence.Repositories;
using MyCSharp.HttpUserAgentParser.DependencyInjection;
using ProjectLibrary.Interfaces;
using ProjectLibrary.Realizations;
using Prometheus;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Configure Serilog using the library
builder.Host.UseSerilog();

var port = Environment.GetEnvironmentVariable("PROMETHEUS_PORT")!;
// Add services to the container
builder.Services.AddApplication().AddPersistence().AddInfrastructure().AddHttpUserAgentParser();
builder.Services.AddSingleton<ILoggerManager>(provider =>
    new LoggerManager(provider.GetRequiredService<IConfiguration>()));
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddTransient<ILinkRepository, RedisLinkLinkRepository>();
builder.Services.AddMonitoring(builder.Configuration, port);

var app = builder.Build();

// Configure the HTTP request pipeline
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseDeveloperExceptionPage();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.UseMetricServer();
app.UseHttpMetrics();

try
{
    var logger = app.Services.GetRequiredService<ILoggerManager>();
    logger.LogInfo("Starting up");
    logger.LogInfo($"Prometheus metrics are available at port {port}");
    app.Run();
}
catch (Exception ex)
{
    var logger = app.Services.GetRequiredService<ILoggerManager>();
    logger.LogError($"Application start-up failed: {ex.Message}");
    throw;
}
finally
{
    Log.CloseAndFlush();
}