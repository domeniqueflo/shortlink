using Microsoft.AspNetCore.Mvc;

namespace MainHandler.Api.Controllers;

[ApiController]
[Route("/")]
public class CreationPageRiderect : ControllerBase
{
    [HttpGet]
    public IActionResult Index()
    {
        return Redirect("http://localhost");
    }
}