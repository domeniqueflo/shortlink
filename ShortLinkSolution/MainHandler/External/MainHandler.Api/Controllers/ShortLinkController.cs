using MainHandler.Application.InfrastructureServicesAbstractions;
using MainHandler.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using ProjectLibrary.Realizations;

namespace MainHandler.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class ShortLinkController : Controller
{
    private readonly ILinkService _linkService;
    private readonly IRequestParser _requestParser;
    private readonly IQueueService _queueService;

    public ShortLinkController(ILinkService linkService, IRequestParser parser, IQueueService queueService)
    {
        _linkService = linkService;
        _requestParser = parser;
        _queueService = queueService;
    }

    [HttpGet("/{shortCode}")]
    public IActionResult RedirectToLink(string shortCode)
    {
        var linkByShortCode = _linkService.GetLinkByShortCode(shortCode);
        var userAgentParsedInfo = _requestParser.GenerateRequestIntoIntoMessage(Request, linkByShortCode);

        if (linkByShortCode == null || string.IsNullOrEmpty(userAgentParsedInfo.ReceivedLink))
        {
            _queueService.SendErrorData(new ErrorData(DateTime.Now.Ticks, userAgentParsedInfo.ShortLink));
            return Redirect("http://localhost/404");
        }

        var resultLink = new UriBuilder(userAgentParsedInfo.ReceivedLink).Uri.AbsoluteUri;

        _queueService.SendAnalyticsData(userAgentParsedInfo);
        
        return Redirect(resultLink);
    }

    [HttpPost("add")]
    public IActionResult CreateTestRecord([FromBody] Link link)
    {
#if DEBUG
        _linkService.Add(link);
#endif
        return Ok(link);
    }
}