﻿using MainHandler.Application.DomainServices;
using MainHandler.Application.InfrastructureServicesAbstractions;
using MainHandler.Infrastructure.InfrastructureServices;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using MyCSharp.HttpUserAgentParser.Providers;
using ProjectLibrary.Realizations;

namespace MainHandler.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<ILinkService, LinkService>();
        serviceCollection.AddScoped<IHttpUserAgentParserProvider, HttpUserAgentParserDefaultProvider>();
        serviceCollection.AddScoped<IRequestParser, RequestParser>();
        serviceCollection.AddScoped<IQueueService, QueueService>();
        serviceCollection.AddMassTransit(mt => mt.AddMassTransit(x =>
        {
            x.UsingRabbitMq((cntxt, cfg) =>
            {
                cfg.Host("rabbitmq", "/", c =>
                {
                    c.Username(Environment.GetEnvironmentVariable("rabbitUser")!);
                    c.Password(Environment.GetEnvironmentVariable("rabbitPass")!);
                });
                
                cfg.Message<RequestInfoData>(messageTopologyConfigurator =>
                {
                    messageTopologyConfigurator.SetEntityName(Environment.GetEnvironmentVariable("analyticsQueue")!);
                });
                cfg.Message<ErrorData>(messageTopologyConfigurator =>
                {
                    messageTopologyConfigurator.SetEntityName(Environment.GetEnvironmentVariable("errorQueue")!);
                });

                cfg.ConfigureEndpoints(cntxt);
            });
        }));
        serviceCollection.AddMassTransitHostedService();

        return serviceCollection;
    }
}