﻿using MainHandler.Application.InfrastructureServicesAbstractions;
using MassTransit;
using ProjectLibrary.Interfaces;
using ProjectLibrary.Realizations;

namespace MainHandler.Infrastructure.InfrastructureServices;

public class QueueService : IQueueService
{
    public QueueService(IPublishEndpoint bus)
    {
        _bus = bus;
    }
    
    private readonly IPublishEndpoint _bus;
    
    public async Task SendAnalyticsData(RequestInfoData requestInfoData)
    {
        await _bus.Publish(requestInfoData);
    }    
    
    public async Task SendErrorData(ErrorData errorData)
    {
        await _bus.Publish(errorData);
    }
} 