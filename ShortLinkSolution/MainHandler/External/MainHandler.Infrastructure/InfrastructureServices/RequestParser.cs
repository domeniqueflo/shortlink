using MainHandler.Application.InfrastructureServicesAbstractions;
using MainHandler.Domain.Entities;
using Microsoft.AspNetCore.Http;
using MyCSharp.HttpUserAgentParser;
using MyCSharp.HttpUserAgentParser.Providers;
using ProjectLibrary.Realizations;

namespace MainHandler.Infrastructure.InfrastructureServices;

public class RequestParser : IRequestParser
{
    public IHttpUserAgentParserProvider AgentParserInstance { get; set; } = new HttpUserAgentParserDefaultProvider();

    public RequestInfoData GenerateRequestIntoIntoMessage(HttpRequest requestInfo, Link? link)
    {
        var userAgentInfoString = requestInfo.Headers.UserAgent;
        HttpUserAgentInformation parsedAgentInfo = default;
        
        if (!string.IsNullOrEmpty(userAgentInfoString))
            parsedAgentInfo = AgentParserInstance.Parse(userAgentInfoString!);
        
        var language = requestInfo.Headers.AcceptLanguage;
        var browserInfo = parsedAgentInfo.UserAgent;
        var shortLink = requestInfo.Path.ToString().Replace("/","");
        string? receivedLink = null;
        string userId = link?.UserId ?? "";

        var isMobile = parsedAgentInfo.Platform?.PlatformType switch
        {
            HttpUserAgentPlatformType.Android or
                HttpUserAgentPlatformType.IOS or
                HttpUserAgentPlatformType.BlackBerry or
                HttpUserAgentPlatformType.Symbian => true,
            _ => false,
        };
        
        if (link != null)
        {
            receivedLink = isMobile
                ? link.MobileLink
                : link.WebLink;
        }

        var requestInfoDto = new RequestInfoData(
            DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
            shortLink,
            language,
            "",
            "",
            browserInfo,
            userId,
            isMobile,
            receivedLink
        );

        return requestInfoDto;
    }
}