﻿using MainHandler.Domain.Entities;

namespace MainHandler.Domain.RepositoriesAbstractions;

public interface ILinkRepository
{
    /// <summary>
    /// Get link info by short code from repository
    /// </summary>
    /// <param name="shortCode"></param>
    /// <returns></returns>
    Link? GetLinkByShortCode(string shortCode);

#if DEBUG

    void Add(Link linkToAdd);

#endif
}