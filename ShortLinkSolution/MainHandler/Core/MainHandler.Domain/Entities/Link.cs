﻿namespace MainHandler.Domain.Entities;

public class Link
{
    /// <summary>
    /// Уникальный идентификатор ссылки
    /// </summary>
    public int Id { get; set; }
    
    /// <summary>
    /// Короткая ссылка
    /// </summary>
    public string ShortLink { get; set; }
    
    /// <summary>
    /// Полная ссылка на веб (по-умочанию)
    /// </summary>
    public string WebLink { get; set; }
    
    /// <summary>
    /// Полная ссылка на открытие с мобильных клиентов
    /// </summary>
    public string? MobileLink { get; set; }
    
    /// <summary>
    /// ID пользователя
    /// </summary>
    public string UserId { get; set; }
}