﻿using MainHandler.Application.InfrastructureServicesAbstractions;
using MainHandler.Domain.Entities;
using MainHandler.Domain.RepositoriesAbstractions;
using ProjectLibrary.Realizations;

namespace MainHandler.Application.DomainServices;

public class LinkService : ILinkService
{
    private readonly ILinkRepository _linkRepository;

    public LinkService(ILinkRepository linkRepository)
    {
        _linkRepository = linkRepository;
    }
    
    /// <summary>
    /// Get Link entity from repository by short code
    /// </summary>
    /// <param name="shortCode">Not long code</param>
    /// <returns></returns>
    public Link GetLinkByShortCode(string shortCode)
    {
        return _linkRepository.GetLinkByShortCode(shortCode);
    }

    public RequestInfoData ParseUserAgent()
    {
        throw new NotImplementedException();
    }

    public void SendErrorData(ErrorData error)
    {
        throw new NotImplementedException();
    }
    
#if DEBUG
    
    public void Add(Link linkToAdd)
    {
        _linkRepository.Add(linkToAdd);
    }
    
#endif    
}