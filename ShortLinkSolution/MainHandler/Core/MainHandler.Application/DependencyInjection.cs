﻿using MainHandler.Application.DomainServices;
using MainHandler.Application.InfrastructureServicesAbstractions;
using Microsoft.Extensions.DependencyInjection;
using ProjectLibrary.Interfaces;
using ProjectLibrary.Realizations;

namespace MainHandler.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddTransient<ILinkService, LinkService>();
        return serviceCollection;
    }
}