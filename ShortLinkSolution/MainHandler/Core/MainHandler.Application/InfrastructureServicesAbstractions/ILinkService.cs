﻿using MainHandler.Domain.Entities;
using ProjectLibrary.Realizations;

namespace MainHandler.Application.InfrastructureServicesAbstractions;

public interface ILinkService
{
    //TODO: Change type to string or URI! 👷
    public Link? GetLinkByShortCode(string shortCode);

    public RequestInfoData ParseUserAgent();

    public void SendErrorData(ErrorData error);
    
#if DEBUG

    public void Add(Link linkToAdd);

#endif
}