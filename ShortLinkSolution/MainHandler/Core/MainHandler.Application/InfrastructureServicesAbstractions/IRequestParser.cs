using MainHandler.Domain.Entities;
using MyCSharp.HttpUserAgentParser.Providers;
using Microsoft.AspNetCore.Http;
using ProjectLibrary.Realizations;

namespace MainHandler.Application.InfrastructureServicesAbstractions;

public interface IRequestParser
{
    protected IHttpUserAgentParserProvider AgentParserInstance { get; set; }
    
    public RequestInfoData GenerateRequestIntoIntoMessage(HttpRequest requestInfo, Link? link);
}