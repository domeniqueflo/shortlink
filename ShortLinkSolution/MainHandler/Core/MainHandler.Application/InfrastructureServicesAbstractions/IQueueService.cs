﻿using ProjectLibrary.Realizations;

namespace MainHandler.Application.InfrastructureServicesAbstractions;

public interface IQueueService
{
    public Task SendAnalyticsData(RequestInfoData requestInfoData);

    public Task SendErrorData(ErrorData errorData);
}