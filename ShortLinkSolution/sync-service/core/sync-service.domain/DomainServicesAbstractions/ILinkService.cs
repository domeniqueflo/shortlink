﻿using sync_service.domain.Entities;

namespace sync_service.domain.DomainServicesAbstractions
{
    public interface ILinkService
    {
        Task CreateLinkAsync(Link link);
        Task DisableLinkAsync(string ShrotLink);
    }
}
