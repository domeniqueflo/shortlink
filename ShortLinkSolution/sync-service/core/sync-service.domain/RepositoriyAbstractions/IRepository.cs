﻿using sync_service.domain.Entities;

namespace sync_service.domain.RepositoriyAbstractions
{
    public interface IRepository
    {
        Task<Link?> GetLinkByShortLinkAsync(string shortLink);
        Task CreateLinkAsync(Link link);
    }
}
