﻿namespace sync_service.domain.Entities.Base
{
    public interface IEntity<Tid>
    {
        public Tid Id { get; set; }
    }
}
