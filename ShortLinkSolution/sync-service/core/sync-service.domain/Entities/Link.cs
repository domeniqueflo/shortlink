﻿using sync_service.domain.Entities.Base;

namespace sync_service.domain.Entities
{
    public class Link : IEntity<int>
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string ShortLink { get; set; }
        public string WebLink { get; set; }
        public string MobileLink { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateDelete { get; set; }
        public LinkStatus Status { get; set; }        
        public bool IsPremium { get; set; } 
    }
}
