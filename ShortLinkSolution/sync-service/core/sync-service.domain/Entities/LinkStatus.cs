﻿using sync_service.domain.Entities.Base;

namespace sync_service.domain.Entities
{
    public class LinkStatus : IEntity<int>
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
