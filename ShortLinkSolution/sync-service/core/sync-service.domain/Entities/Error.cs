﻿using sync_service.domain.Entities.Base;

namespace sync_service.domain.Entities
{
    public class Error : IEntity<int>
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string ShortLink { get; set; }
    }
}
