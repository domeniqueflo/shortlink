using ProjectLibrary.Interfaces;
//using ProjectLibrary.Realizations;
//using Prometheus;
using Serilog;
//using Serilog.Sinks.Grafana.Loki;
using sync_service.infrastructure.Workers;

//var port = Environment.GetEnvironmentVariable("PROMETHEUS_PORT")!;
var builder = Host.CreateApplicationBuilder(args);
builder.Services.AddHostedService<Worker>();

// Set Serilog as the default logger
builder.Logging.ClearProviders();

// Add custom logger manager to DI
builder.Services.AddSingleton<ILoggerManager>(provider =>
    new LoggerManager(provider.GetRequiredService<IConfiguration>()));
Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(builder.Configuration)
    .CreateLogger();
builder.Logging.AddSerilog(Log.Logger, dispose: true);

builder.Services.AddHostedService<Worker>();
//builder.Services.AddMonitoring(builder.Configuration, port);

var host = builder.Build();

try
{
    var logger = Log.Logger;
    //var metricServer = host.Services.GetRequiredService<IMetricServer>();
    //metricServer.Start();
    logger.Information("Starting up");
    host.Run();
}
catch (Exception ex)
{
    var logger = Log.Logger;
    logger.Error($"Application start-up failed: {ex.Message}");
    throw;
}
finally
{
    Log.CloseAndFlush();
}

