﻿using Microsoft.Extensions.DependencyInjection;
using sync_service.infrastructure.Workers;

namespace sync_service.infrastructure
{
    public static class DependencyInjecton
    {
        public static IServiceCollection AddApplication(this IServiceCollection service)
        {
            service.AddHostedService<Worker>();
            return service;
        }
    }
}
