export interface LinkModelData {
    creationDate: string,
    expirationDate: string,
    id: number,
    isPremium: boolean,
    linkStatusId: number,
    mobileLink: string,
    shortLink: string,
    userId: string,
    webLink: string
}
    
