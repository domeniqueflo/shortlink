export interface RegisterUserModel {
    email: string;
    password: string;
    confirmpassword: string;
  }