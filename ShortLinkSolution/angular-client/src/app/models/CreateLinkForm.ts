export interface LinkFormModel {
    webLink: string,
    mobileLink: string
}