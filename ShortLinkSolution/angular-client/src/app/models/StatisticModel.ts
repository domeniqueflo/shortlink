export interface StatisticModel {
    shortLink: string;
    webLink: string,
    mobileLink: string,
    date: string;
    clicks: number;
}