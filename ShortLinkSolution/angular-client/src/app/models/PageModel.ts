export interface PageModel {
    url: string;
    name: string;
    icon: string;
    child?: Array<{name: string}>
}