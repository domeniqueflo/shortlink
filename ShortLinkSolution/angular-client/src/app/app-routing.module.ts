import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './presentation/login-page/login-page.component';
import { AuthLayoutComponent } from './presentation/layouts/auth-layout/auth-layout.component';
import { RegisterPageComponent } from './presentation/register-page/register-page.component';
import { SiteLayoutComponent } from './presentation/layouts/site-layout/site-layout.component';
import { AuthGuard } from './services/auth.guard';
import { OverviewPageComponent } from './presentation/overview-page/overview-page.component';
import { ShortLinkPageComponent } from './presentation/short-link-page/short-link-page.component';
import { ErrorPageComponent } from './presentation/error-page/error-page.component';
const routes: Routes = [
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      { path: '', redirectTo: '/login', pathMatch: 'full' },
      { path: 'login', component: LoginPageComponent },
      { path: 'register', component: RegisterPageComponent },
      { path: '404', component: ErrorPageComponent }
    ]
  },
  {
    path: '',
    component: SiteLayoutComponent,
    //canActivate: [AuthGuard],
    children: [
      { path: 'overview', component: OverviewPageComponent },
      { path: 'links', component: ShortLinkPageComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
