import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  userEmail: string = '';

  constructor(private authService: AuthService) {
    
  }

  ngOnInit(): void {
      let userData = this.authService.getUserInfoData();
      if(userData != null){
        this.userEmail = userData;
      }
  }
}
