import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageModel } from 'src/app/models/PageModel';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit{
  links: Array<PageModel> = [];

  constructor(private authService: AuthService, private router: Router) {
    
  }

  ngOnInit(): void {
      this.links = [
        { url: '/overview', name: 'Analytics', icon: 'dashboard'},
        { url: '/links', name: 'Short links', icon: 'compare_arrows'},
        //{ url: '/users', name: 'Profile', icon: 'account_circle'},
      ]
  }

  logOut(event: Event): void {
    this.authService.logout();
    this.router.navigate(['/']);
  }
}
