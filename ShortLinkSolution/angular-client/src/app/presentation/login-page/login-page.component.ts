import { Component,  OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { MaterializeService } from 'src/app/services/materialize.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent implements OnDestroy, OnInit {

  loginForm: FormGroup;
  aSub: Subscription;

  constructor(private authService: AuthService, 
    private router: Router,
    builder: FormBuilder) {

    this.loginForm = builder.group({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)])
    });

    this.aSub = new Subscription();
  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    if(this.aSub){
      this.aSub.unsubscribe();
    };
  }

  onSubmit(): void {
    this.loginForm.disable();
    this.aSub = this.authService.login(this.loginForm.value).subscribe(
      resp => {
        this.authService.setToken(resp.access_token);
        this.authService.getUserInfo()
          .subscribe(
            resp => {     
              this.router.navigate(["/overview"]);
              this.authService.setUserInfoData(resp.value);
            }
          );
      },
      error => {
        this.loginForm.enable();
          MaterializeService.toast(error.message);        
      }
    )   
  }
}
