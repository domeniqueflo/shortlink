import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { LinkFormModel } from 'src/app/models/CreateLinkForm';

import { LinkModelData } from 'src/app/models/LinkModel';
import { LinkService } from 'src/app/services/link.service';
import { MaterializeService } from 'src/app/services/materialize.service';


@Component({
  selector: 'app-short-link-page',
  templateUrl: './short-link-page.component.html',
  styleUrls: ['./short-link-page.component.scss']
})
export class ShortLinkPageComponent implements AfterViewInit, OnInit{
  @ViewChild('createModal') createModal!: ElementRef;
  
  createLinkForm: FormGroup;
  aSub: Subscription;
  links$: Observable<LinkModelData[]>;

  constructor(builder: FormBuilder, private linkService: LinkService) {
    this.createLinkForm = builder.group({
      webLink: new FormControl(null),
      mobileLink: new FormControl(null),
      utmSource: new FormControl(null),
      utmMedium: new FormControl(null),
      utmCampaing: new FormControl(null)
    });
    this.aSub = new Subscription();
    this.links$ = new Observable<any>;
  }

  ngOnInit(): void {
   this.links$ = this.linkService.links$;
  }

  ngAfterViewInit(): void {
      MaterializeService.InitializeCreateButton(this.createModal);
  }

  onSubmit(): void {
    let resultWebUrl = this.utmConstructor(this.createLinkForm.value.webLink, this.createLinkForm);
    let resultMobileUrl: string;
    if(this.createLinkForm.value.mobileLink) {
      resultMobileUrl = this.utmConstructor(this.createLinkForm.value.mobileLink, this.createLinkForm);
    }
    else {
      resultMobileUrl = this.createLinkForm.value.mobileLink
    };
    
    let formData: LinkFormModel = 
    {
      webLink: resultWebUrl,
      mobileLink: resultMobileUrl
    };

    this.linkService.create(formData).subscribe(
      res => {
        console.log(res);
        this.linkService.update();
      },
      error => {
        console.log(error);
        this.linkService.update();
      }
    );
  }
  
  utmConstructor(link: string, form: FormGroup): string {
    let resultLink: string = link;
    if(form.value.utmSource){
      resultLink += `?utm_source=${form.value.utmSource}`;
    }
    if(form.value.utmMedium){
      resultLink += `&utm_medium=${form.value.utmMedium}`;
    }
    if(form.value.utmCampaing){
      resultLink += `&utm_campaign=${form.value.utmCampaing}`;
    }
    return resultLink;
  }
}
