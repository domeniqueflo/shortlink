import { Component, OnDestroy } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { MaterializeService } from 'src/app/services/materialize.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnDestroy {

  registerForm: FormGroup;
  aSub: Subscription;

  constructor(private router: Router,
    private authService: AuthService,
    builder: FormBuilder) {
    
    this.registerForm = builder.group({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      confirm_password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      agree: new FormControl(null, [Validators.required])
    },
    {
      validator: this.passwordMatchValudator
    });

    this.aSub = new Subscription();
  }

  ngOnDestroy(): void {
    if (this.aSub) {
      this.aSub.unsubscribe();
    } 
  }

  passwordMatchValudator(control: AbstractControl) {
    return control.get("password")?.value === control.get("confirm_password")?.value
      ? null : { mismatch: true };
  }

  onSubmit(){
    this.registerForm.disable();
    this.aSub = this.authService.register(this.registerForm.value).subscribe(
      resp => {
        this.router.navigate(['/login']);
      },
      error => {
        this.registerForm.enable();
        MaterializeService.toast(error.message);
      }
    )
  }
}
