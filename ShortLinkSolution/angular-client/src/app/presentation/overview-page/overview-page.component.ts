import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as Highcharts from 'highcharts';
import { BehaviorSubject, map, Observable, Subscription } from 'rxjs';
import { StatisticModel } from 'src/app/models/StatisticModel';
import { ChartDataService } from 'src/app/services/chart.data.service';

@Component({
  selector: 'app-overview-page',
  templateUrl: './overview-page.component.html',
  styleUrls: ['./overview-page.component.scss']
})
export class OverviewPageComponent implements OnInit, OnDestroy {
  Highcharts: typeof Highcharts = Highcharts;
  private subscription: Subscription;
  data: BehaviorSubject<StatisticModel[]> = new BehaviorSubject<StatisticModel[]>([])
  data$: Observable<StatisticModel[]> = this.data;
  tableData: BehaviorSubject<StatisticModel[]> = new BehaviorSubject<StatisticModel[]>([])
  tableData$: Observable<StatisticModel[]> = this.tableData;
  dataRangeFliter: FormGroup;
  chartOptions: Highcharts.Options = {};
  actualLink: string = '';

  constructor(private chartDataService: ChartDataService, private buider: FormBuilder)
  {
    this.subscription = new Subscription();
    this.dataRangeFliter = buider.group({
      dateFrom: new FormControl(null, [Validators.required]),
      dateTo: new FormControl(null, [Validators.required])
    });


    this.subscription = this.chartDataService.getStat()
    .subscribe(
      val => {
        this.data.next(val);
        this.agregateLinkStat(val);
      }
    );
  };
  

  createChartOption(categories: string[], clicks: number[], linkName: string) {
    this.chartOptions = {
      title: {
        text: 'Link clicks'
      },
      xAxis: {
        categories: categories
      },
      yAxis: {
        title: {
         text: 'Clicks'
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        borderWidth: 0,
     },
      series: [{
        name: linkName,
        type: 'line',
         data: clicks
       }]
    };
  };

  ngOnInit() {
    console.log(this.data$);
    this.subscription = this.chartDataService.getStat().subscribe(val => {
      this.data.next(val);
      this.updateChartData();
    });
  }

  
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    };
  }
    
  updateChart(linkName: string) {
    this.dataRangeFliter.reset();
    this.chartDataService.getStat().subscribe(val => {
      this.data.next(val);
    });

    if(linkName != null) {
      this.data.next(this.data.value.filter(el => el.shortLink == linkName))
        let categories = this.data.value.map(el => el.date);
        let clicks = this.data.value.map(el => el.clicks);
        this.createChartOption(categories, clicks, linkName);
        this.actualLink = linkName;
    };
  };

  updateChartData() {
    let categories = this.data.value.map(el => el.date);
    let clicks = this.data.value.map(el => el.clicks);
    this.createChartOption(categories, clicks, this.actualLink);
  }
  
  onSubmit(): void{
    if(this.dataRangeFliter.valid){
      this.chartDataService.reloadDataRangeData(
        this.dataRangeFliter.value.dateFrom,
        this.dataRangeFliter.value.dateTo
      );
    }
  }

  agregateLinkStat(data: StatisticModel[]): void {
    let linkList: Array<string> = [];
    let dataTotableRendering: Array<StatisticModel> = [];
    data.forEach(item => linkList.push(item.shortLink));

    Array.from(new Set(linkList)).forEach(l => {
        let linkClicks = data.filter(c => c.shortLink == l).reduce((a, b) => a + b.clicks, 0);
        dataTotableRendering.push({
          shortLink: l,
          date: "",
          clicks: linkClicks,
          webLink: data.find(i => i.shortLink == l)?.webLink,
          mobileLink: data.find(i => i.shortLink == l)?.mobileLink,
        } as StatisticModel)
        console.log({
          shortLink: l,
          date: "",
          clicks: linkClicks,
          webLink: data.find(i => i.shortLink == l)?.webLink,
          mobileLink: data.find(i => i.shortLink == l)?.mobileLink,
        })
      })

      this.tableData.next(dataTotableRendering);
  }
}