import { Injectable } from "@angular/core";


@Injectable({
    providedIn: 'root'
})

export class ApiDefinitionConstants {
    apiGateway: string = "http://localhost";
    userServiceSignUp: string = `${this.apiGateway}/api/v1/user-service/user/signup`;
    userServiceSignIn: string = `${this.apiGateway}/api/v1/user-service/user/signin`;
    userServiceUserInfo: string = `${this.apiGateway}/api/v1/user-service/user/claim`;
    getAnalyticsData: string = `${this.apiGateway}/api/v1/analytics-service/statistic`;
    pseudoAnalyticsStatistic: string = `${this.apiGateway}/api/v1/analytics-service/statistic/GetPseudoAnalyticsData`;
    linksServiceCreateLink: string = `${this.apiGateway}/api/v1/link-service/links/create`;
    linksServiceGetLinks: string = `${this.apiGateway}/api/v1/link-service/links/getalluserlink`;
}
