import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiDefinitionConstants } from '../storage/api.definition.constants';
import { StatisticModel } from '../models/StatisticModel';

export interface LinkStat {
    shortLink: string;
    data: Record<string, number>[]
}

@Injectable({
    providedIn: 'root'
})
export class ChartDataService {
    private chartDataSubject: BehaviorSubject<number[]> = new BehaviorSubject<number[]>([29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]);
    chartData$: Observable<number[]> = this.chartDataSubject.asObservable();
    url: string = '';
    private statisticDataSubject: BehaviorSubject<StatisticModel[]> = new BehaviorSubject<StatisticModel[]>([]);
    statisticData$: Observable<StatisticModel[]> = this.statisticDataSubject;

    constructor(private client: HttpClient, 
        private apiDefinition: ApiDefinitionConstants) 
    {

        if(localStorage.getItem("toggle") == 'demo'){
            this.url = this.apiDefinition.pseudoAnalyticsStatistic;
        }
        else {
            this.url = this.apiDefinition.getAnalyticsData;
        }

        this.client.get<StatisticModel[]>(this.url)
        .subscribe
        (
            val => 
            {
                this.statisticDataSubject.next(val)
                console.log("form api", val);
            }            
        )
    };
    
    getStat(): Observable<StatisticModel[]> {
       
        return this.statisticData$;
    } 

    updateData(newData: StatisticModel[]): void {
        console.log("fom update", newData);
        this.statisticDataSubject.next(newData);
    }

    reloadDataRangeData(dateFrom: string, dateTo:string): void {
        this.client.get<StatisticModel[]>(`${this.url}?dateFrom=${dateFrom}&dateTo=${dateTo}`)
        .subscribe
        (
            val => 
            {
                this.statisticDataSubject.next(val)
                console.log("form api", val);
            }            
        )
    }

    
}


