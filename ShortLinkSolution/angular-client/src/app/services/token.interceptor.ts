import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs";



@Injectable()
export class TokenIntercepter implements HttpInterceptor {

    constructor(private authService: AuthService) {
        
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.authService.isAuthenticated()) {
            req = req.clone({
              headers: req.headers.set('Authorization', 'Bearer ' + this.authService.getToken())
            });
        }
        return next.handle(req);
    }
}