import { ElementRef, NgModule } from "@angular/core";

declare var M:any;

@NgModule()
export class MaterializeService {
    static toast(message: string) {
        M.toast({html: message});
    }

    static InitializeCreateButton(ref: ElementRef): void{
        M.Modal.init(ref?.nativeElement);
    }

}