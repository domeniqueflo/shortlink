import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from "@angular/router";
import { Observable, of } from "rxjs";


@Injectable({
    providedIn: 'root'
})

export class AuthGuard {
    constructor(private authService: AuthService, 
        private router: Router) {

    }

    canActivate(router: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        if (this.authService.isAuthenticated()) {
            return of(true)
        } 
        else {
            this.router.navigate(['/login'], {
              queryParams: {
                accsessDenied: true
              }
            })
            return of(false);
        }
    }

    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.canActivate(childRoute, state);
      }
}