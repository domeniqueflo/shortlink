import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { RegisterUserModel } from "../models/RegisterUserModel";
import { Observable, tap } from "rxjs";
import { ApiDefinitionConstants } from "../storage/api.definition.constants";
import { LoginUserModel } from "../models/LoginUserModel";
import { UserClaim } from "../models/UserClaim";


@Injectable({
    providedIn: 'root'
})

export class AuthService {
    private token: string;
    constructor(private client: HttpClient, private apiDefinition: ApiDefinitionConstants) {
        this.token  = '';
    }

    register(user: RegisterUserModel): Observable<{ token: string }>{
        return this.client.post<{ token: string }>(this.apiDefinition.userServiceSignUp, user)
    }

    login(user: LoginUserModel): Observable<any>{
        return this.client.post<any>(this.apiDefinition.userServiceSignIn, user)
    }
    
    setToken(token: string): void {
        sessionStorage.setItem("auth-token", token);
    }

    getToken(): string | null {
        return sessionStorage.getItem("auth-token") ?? null;
    }

    isAuthenticated(): boolean {
        return !! sessionStorage.getItem("auth-token");
    }

    getUserInfo(): Observable<any> {
        return this.client.get<any>(this.apiDefinition.userServiceUserInfo);
    }

    setUserInfoData(data: string): void {
        sessionStorage.setItem("user-data", data);
    }

    getUserInfoData(): string | null {
        return sessionStorage.getItem("user-data");
    }

    logout(): void {
        sessionStorage.removeItem("user-data");
        sessionStorage.removeItem("auth-token");
    }
}