import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { ApiDefinitionConstants } from "../storage/api.definition.constants";
import { LinkModelData } from "../models/LinkModel";
import { LinkFormModel } from "../models/CreateLinkForm";

@Injectable({
    providedIn: 'root'
})

export class LinkService {

    links: BehaviorSubject<LinkModelData[]> = new BehaviorSubject<LinkModelData[]>([]);
    links$: Observable<LinkModelData[]> = this.links;
    constructor(private client: HttpClient, private apiDefinition: ApiDefinitionConstants) {
        this.update();      
    }

   async update(): Promise<void> {
        this.client.get<LinkModelData[]>(this.apiDefinition.linksServiceGetLinks)
        .subscribe(
            resp => {
                this.links.next(resp);
            }
        )
    }

    create(createLinkRequest: LinkFormModel): Observable<string> {
        return this.client.post<string>(this.apiDefinition.linksServiceCreateLink, createLinkRequest)
    }

    getLinks(): Observable<LinkModelData[]> {
        return this.links$;
    }
}