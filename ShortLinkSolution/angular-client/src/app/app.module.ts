import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HighchartsChartModule } from 'highcharts-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './presentation/login-page/login-page.component';
import { SiteLayoutComponent } from './presentation/layouts/site-layout/site-layout.component';
import { AuthLayoutComponent } from './presentation/layouts/auth-layout/auth-layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './presentation/shared-components/navbar/navbar.component';
import { RegisterPageComponent } from './presentation/register-page/register-page.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { SidebarComponent } from './presentation/shared-components/sidebar/sidebar.component'
import { TokenIntercepter } from './services/token.interceptor';
import { OverviewPageComponent } from './presentation/overview-page/overview-page.component';
import { ShortLinkPageComponent } from './presentation/short-link-page/short-link-page.component';
import { ErrorPageComponent } from './presentation/error-page/error-page.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    SiteLayoutComponent,
    AuthLayoutComponent,
    NavbarComponent,
    RegisterPageComponent,
    SidebarComponent,
    OverviewPageComponent,
    ShortLinkPageComponent,
    ErrorPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HighchartsChartModule,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenIntercepter,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
