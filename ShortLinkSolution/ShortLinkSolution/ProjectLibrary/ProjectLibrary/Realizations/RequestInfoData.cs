using ProjectLibrary.Interfaces;

namespace ProjectLibrary.Realizations;

public sealed record RequestInfoData(
    long RequestDateTime,
    string ShortLink,
    string Language,
    string Country,
    string City,
    string BrowserInfo,
    int UserId,
    bool IsMobile,
    string? ReceivedLink) : IRequestInfoData;