namespace ProjectLibrary.Interfaces;

public interface IErrorData
{
    long ErrorDateTime { get; init; }
    
    string ShortLink { get; init; }
}