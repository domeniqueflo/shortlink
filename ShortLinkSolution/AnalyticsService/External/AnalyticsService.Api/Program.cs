using AnalyticsService.Application;
using AnalyticsService.Infrastructure;
using AnalyticsService.Persistence;
using MassTransit.Logging;
using ProjectLibrary.Interfaces;
using ProjectLibrary.Realizations;
using Prometheus;
using Serilog;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer("Bearer", options =>
    {
        options.Authority = "http://identity-service:8080";
        options.Audience = "analytics-service";
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false
        };
        options.RequireHttpsMetadata = false;
    });
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddApplication().AddPersistence().AddInfrastructure();
builder.Services.AddSingleton<ILoggerManager>(provider =>
    new LoggerManager(provider.GetRequiredService<IConfiguration>()));
builder.Services.AddMonitoring(builder.Configuration, Environment.GetEnvironmentVariable("PROMETHEUS_PORT")!);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseDeveloperExceptionPage();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.UseMetricServer();

app.UseHttpMetrics();

try
{
    var dbContext = new ClickHouseDbContext();
    DataBaseInitializer dbi = new DataBaseInitializer(dbContext);
    await dbi.Initialize();
}
catch
{

}

try
{
    var logger = app.Services.GetRequiredService<ILoggerManager>();
    logger.LogInfo("Starting up");
    app.Run();
}
catch (Exception ex)
{
    var logger = app.Services.GetRequiredService<ILoggerManager>();
    logger.LogError($"Application start-up failed: {ex.Message}");
    throw;
}
finally
{
    Log.CloseAndFlush();
}

