﻿using AnalyticsService.Domain.DomainServicesAbstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using ProjectLibrary.Realizations;
using System;
using System.Globalization;
using System.Security.Claims;

namespace AnalyticsService.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class StatisticController : ControllerBase
    {
        private readonly ILogger<StatisticController> _logger;
        private readonly IStatisticService _statisticService;
        private Random _random;

        public StatisticController(ILogger<StatisticController> logger, IStatisticService statisticService)
        {
            _logger = logger;
            _statisticService = statisticService;
            _random = new Random();
        }

        [HttpGet]
        public async Task<IActionResult> GetAnalyticsData([FromQuery] string? dateFrom, string? dateTo)
        {
            DateTime toDT = DateTime.Now;
            DateTime fromDT = toDT.AddDays(-30);

            if (!dateFrom.IsNullOrEmpty())
            {
                toDT = DateTime.ParseExact(dateTo, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                fromDT = DateTime.ParseExact(dateFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }

            string userId = User.FindFirst(ClaimTypes.NameIdentifier)!.Value;


            DateTime dateTimeNow = DateTime.Now;
            AnaliticsDataRangeModel dataRange = new AnaliticsDataRangeModel(fromDT, toDT);

            var result = await _statisticService.GetLinkStatisticByUserIdAsync(userId, dataRange);

            List<string> shortLinks = result.Select(el => el.ShortLink).Distinct().ToList();

            List<ShortStatisticModel> finalResult = new List<ShortStatisticModel>();


            foreach (string shortLink in shortLinks)
            {
                var shortLinkInfo = result.Where(el => el.ShortLink == shortLink).ToList();
                Dictionary<DateTime, ShortStatisticModel> dict = new Dictionary<DateTime, ShortStatisticModel>();

                var forMobileLink = shortLinkInfo.Where(el => el.IsMobile).FirstOrDefault();
                var forWebLink = shortLinkInfo.Where(el => !el.IsMobile).FirstOrDefault();

                string mobileLink = (forMobileLink == null) ? "" : forMobileLink.ReceivedLink!;
                string webLink = (forWebLink == null) ? "" : forWebLink.ReceivedLink!;

                foreach (var info in shortLinkInfo)
                {
                    DateTime infoDT = DateTime.ParseExact(info.RequestDateTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    infoDT = infoDT.Date;

                    if (dict.TryGetValue(infoDT, out var val))
                    {
                        val.Clicks++;
                    }
                    else
                    {
                        var record = new ShortStatisticModel() { ShortLink = shortLink, Date = infoDT.ToString("yyyy-MM-dd"), Clicks = 1, MobileLink = mobileLink, WebLink = webLink };
                        finalResult.Add(record);
                        dict.Add(infoDT, record);
                    }
                }
            }

            return Ok(finalResult);
        }


        [HttpGet("GetPseudoAnalyticsData")]
        public async Task<IActionResult> GetPseudoAnalyticsData([FromQuery] string? dateFrom, string? dateTo)
        {
            DateTime toDT = DateTime.Now;
            DateTime fromDT = toDT.AddDays(-30);

            if (!dateFrom.IsNullOrEmpty())
            {
                toDT = DateTime.ParseExact(dateTo, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                fromDT = DateTime.ParseExact(dateFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }

            List<ShortStatisticModel> finalResult = new List<ShortStatisticModel>();

            int linkCount = _random.Next(2, 6);

            List<string> links = new List<string>() { "google.com", "ya.ru", "mail.ru", "otus.ru", "github.com" };

            for (int i = 0; i < linkCount; i++)
            {
                int length = (toDT - fromDT).Days;

                string shLink = GenerateShortLink();

                string webLink = links[_random.Next(0, links.Count)];
                string mobileLink = links[_random.Next(0, links.Count)];

                while (length >= 0)
                {
                    DateTime dt = toDT.AddDays(-length);
                    length--;

                    finalResult.Add(new ShortStatisticModel()
                    {
                        ShortLink = shLink,
                        Date = dt.ToString("yyyy-MM-dd"),
                        Clicks = _random.Next(100, 501),
                        WebLink = webLink,
                        MobileLink = mobileLink
                    });
                }
            }

            return Ok(finalResult);
        }


        private string GenerateShortLink()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 8)
                .Select(s => s[_random.Next(s.Length)]).ToArray());
        }
    }
}
