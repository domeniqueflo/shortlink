using AnalyticsService.Infrastructure.InfrastructureServices;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;

namespace AnalyticsService.Infrastructure;


public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddMassTransit(mt => mt.AddMassTransit(x =>
        {
            x.AddConsumer<MessageConsumer>();
            x.UsingRabbitMq((cntxt, cfg) =>
            {
                cfg.Host("rabbitmq", "/", c =>
                {
                    c.Username(Environment.GetEnvironmentVariable("rabbitUser")!);
                    c.Password(Environment.GetEnvironmentVariable("rabbitPass")!);
                });

                cfg.ReceiveEndpoint(Environment.GetEnvironmentVariable("analyticsQueue")!, ep =>
                {
                    ep.ConfigureConsumer<MessageConsumer>(cntxt);
                });

                cfg.ConfigureEndpoints(cntxt);
            });
        }));
        serviceCollection.AddSingleton<MessageConsumer>();
        
        return serviceCollection;
    }
}
