using System.Threading.Channels;
using AnalyticsService.Application.InfrastructureServicesAbstractions;
using AnalyticsService.Domain.RepositoriesAbstraction;
using AnalyticsService.Persistence;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProjectLibrary.Realizations;
using Serilog;

namespace AnalyticsService.Infrastructure.InfrastructureServices;

public class MessageConsumer : IMessageConsumer, IConsumer<RequestInfoData>, IDisposable
{
    private readonly ILogger<MessageConsumer> _logger;
    private readonly IServiceProvider _serviceProvider;
    private readonly IAnalyticsRepository _analyticsRepository;
    private readonly Channel<RequestInfoData> _channel;
    private readonly List<RequestInfoData> _buffer = new();
    private readonly Timer _timer;
    private readonly int _bufferLimit = 10;
    private readonly TimeSpan _bufferTimeout = TimeSpan.FromMinutes(1);
    private readonly object _lock = new();
    
    public MessageConsumer(ILogger<MessageConsumer> logger, IServiceProvider serviceProvider)
    {
        _logger = logger;
        _serviceProvider = serviceProvider;
        _channel = Channel.CreateUnbounded<RequestInfoData>();
        _timer = new Timer(FlushBuffer, null, _bufferTimeout, _bufferTimeout);
        Task.Run(ProcessChannel);
    }
    
    public async Task Consume(ConsumeContext<RequestInfoData> context) {
        var message = context.Message;
        _logger.LogInformation($"Message from Producer : {message.ToString()}, {message.BrowserInfo}");
        var analyticsDataRecord = new RequestInfoData(
             message.RequestDateTime,
             message.ShortLink,
             message.Language,
             message.Country,
             message.City,
             message.BrowserInfo,
             message.UserId,
             message.IsMobile,
             message.ReceivedLink 
        );
        
        await _channel.Writer.WriteAsync(analyticsDataRecord);
    }
    
    private async Task ProcessChannel()
    {
        await foreach (var item in _channel.Reader.ReadAllAsync())
        {
            lock (_lock)
            {
                _buffer.Add(item);
                
                if (_buffer.Count < _bufferLimit) 
                    continue;
                
                Log.Warning("💁 Buffer limit reached, flushing buffer");
                FlushBuffer();
            }
        }
    }

    private async void FlushBuffer(object? state = null)
    {
        List<RequestInfoData> itemsToFlush = new();
        
        try
        {
            Log.Information("💾 Saving records to database: {@records}", _buffer.Count);
            lock (_lock)
            {
                if (_buffer.Count == 0)
                {
                    return;
                }
                itemsToFlush = new List<RequestInfoData>(_buffer);
                _buffer.Clear();
            }

            using (var scope = _serviceProvider.CreateScope())
            {
                var analyticsRepository = scope.ServiceProvider.GetRequiredService<IAnalyticsRepository>();
                await analyticsRepository.AddRangeAsync(itemsToFlush);
            }
            _logger.LogInformation("Records successfully saved to database.");
        }
        catch (Exception e)
        {
            _logger.LogError($"⛔ Error saving records to database: {e}");
            foreach (var record in itemsToFlush)
            {
                await _channel.Writer.WriteAsync(record);
            }
            _logger.LogInformation("Records re-queued to channel after failure.");
        }
    }

    public List<RequestInfoData> GetVisitedInformationMessages()
    {
        //ConsumeContext<VisitedInfoMessage>.
        //ConsumerDefinition<>
        
        throw new NotImplementedException();
    }
    
    public void Dispose()
    {
        _timer.Dispose();
    }
}