using AnalyticsService.Application.DomaimServices;
using AnalyticsService.Domain.DomainServicesAbstractions;
using AnalyticsService.Domain.RepositoriesAbstraction;
using AnalyticsService.Persistence.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace AnalyticsService.Persistence;

public static class DependencyInjection
{
    public static IServiceCollection AddPersistence(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<ClickHouseDbContext>();
        serviceCollection.AddScoped<IAnalyticsRepository, ClickAnalyticsRepository>();
        return serviceCollection;
    }
}