﻿using ClickHouse.Client.ADO;
using ClickHouse.Client.Copy;
using ClickHouse.Client.Utility;
using ClickHouse.EntityFrameworkCore.Extensions;
using Microsoft.EntityFrameworkCore;
using ProjectLibrary.Realizations;
using System.Globalization;

namespace AnalyticsService.Persistence
{
    public class ClickHouseDbContext : DbContext
    {
        //private readonly ClickHouseConnection _connection;
        public DbSet<RequestInfoData> AnalyticsTable { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            try
            {
                base.OnConfiguring(optionsBuilder);

                //optionsBuilder.UseClickHouse("Host=localhost;Protocol=http;Port=8123;Database=QuickStart");
                optionsBuilder.UseClickHouse(Environment.GetEnvironmentVariable("connectionString")!);
            }
            catch (Exception ex)
            {

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            try
            {
                var entityTypeBuilder = modelBuilder.Entity<RequestInfoData>();

                //entityTypeBuilder.Property(e => e.request_date).HasColumnName("request_date");
                //entityTypeBuilder.Property(e => e.short_link).HasColumnName("short_link");
                //entityTypeBuilder.Property(e => e.language).HasColumnName("language");
                //entityTypeBuilder.Property(e => e.country).HasColumnName("country");
                //entityTypeBuilder.Property(e => e.city).HasColumnName("city");
                //entityTypeBuilder.Property(e => e.browser_info).HasColumnName("browser_info");
                //entityTypeBuilder.Property(e => e.user_id).HasColumnName("user_id");
                //entityTypeBuilder.Property(e => e.is_mobile).HasColumnName("is_mobile");
                //entityTypeBuilder.Property(e => e.reserved_link).HasColumnName("reserved_link");

                entityTypeBuilder.HasKey(e => new { e.ShortLink, e.RequestDateTime, e.BrowserInfo });

                entityTypeBuilder.ToTable("analytics", table => table
                    .HasMergeTreeEngine()
                    .WithPrimaryKey("ShortLink", "RequestDateTime", "BrowserInfo"));
            }
            catch(Exception ex)
            {

            }
        }

        //public async Task CreateDependsTable()
        //{
        //    var sqlStatement = "CREATE TABLE IF NOT EXISTS " +
        //      "default.analytics (" +
        //          "request_date String," +
        //          "short_link String," +
        //          "language String," +
        //          "country String," +
        //          "city String," +
        //          "browser_info String," +
        //          "user_id String," +
        //          "is_mobile bool," +
        //          "reserved_link String," +

        //      ")" +
        //      "ENGINE = MergeTree()" +
        //      "order by request_date;";

        //    var command = _connection.CreateCommand();
        //    command.CommandText = sqlStatement;
        //    await command.ExecuteNonQueryAsync();
        //}


        //public ClickHouseBulkCopy GetBulkCopy()
        //    => new ClickHouseBulkCopy(_connection) 
        //    {
        //        DestinationTableName = "default.analytics",
        //        ColumnNames = 
        //        [
        //            "request_date", 
        //            "short_link", 
        //            "language", 
        //            "country", 
        //            "city", 
        //            "browser_info", 
        //            "user_id", 
        //            "is_mobile", 
        //            "reserved_link"
        //        ],
        //        BatchSize = 100,
        //        MaxDegreeOfParallelism = 10,
        //    };

       

        public async Task<ICollection<RequestInfoData>?> GetStatisticByUserIdAndDataRangeAsync(string userId, AnaliticsDataRangeModel dataRange)
        {
            var result = AnalyticsTable.Where(new Func<RequestInfoData, bool>(el =>
            {
                if (el.UserId != userId) { return false; }
                DateTime reqDate = DateTime.ParseExact(el.RequestDateTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

                if ((reqDate < dataRange.DateFrom) || (reqDate > dataRange.DateTo)) return false;

                return true;
            }));

            return result.ToList();
        }
    }
}
