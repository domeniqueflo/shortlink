﻿using ClickHouse.Client.ADO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalyticsService.Persistence
{
    public class DataBaseInitializer
    {
        //static ClickHouseConnection _connection = new ClickHouseConnection(Environment.GetEnvironmentVariable(Environment.GetEnvironmentVariable("connectionString")!));

        //public static async Task InitializeClickHouse()
        //{
        //    //var sqlStatement = "CREATE TABLE IF NOT EXISTS " +
        //    //  "default.analytics (" +
        //    //      "request_date String," +
        //    //      "short_link String," +
        //    //      "language String," +
        //    //      "country String," +
        //    //      "city String," +
        //    //      "browser_info String," +
        //    //      "user_id String," +
        //    //      "is_mobile bool," +
        //    //      "reserved_link String," +

        //    //  ")" +
        //    //  "ENGINE = MergeTree()" +
        //    //  "order by request_date;";

        //    //var command = _connection.CreateCommand();
        //    //command.CommandText = sqlStatement;
        //    //await command.ExecuteNonQueryAsync();


        //}

        public ClickHouseDbContext DBContext;
        
        public DataBaseInitializer(ClickHouseDbContext dBContext)
        {
            DBContext = dBContext;
        }

        public async Task Initialize()
        {
            try
            {
                await DBContext.Database.EnsureCreatedAsync();
            }
            catch(Exception ex)
            { 
            
            }
        }
    }
}
