using AnalyticsService.Domain.RepositoriesAbstraction;
using Microsoft.Extensions.Logging;
using ProjectLibrary.Realizations;

namespace AnalyticsService.Persistence.Repositories;

public class ClickAnalyticsRepository : IAnalyticsRepository
{
    private readonly ClickHouseDbContext _context;
    private readonly ILogger<ClickAnalyticsRepository> _logger;

    public ClickAnalyticsRepository(ClickHouseDbContext context, ILogger<ClickAnalyticsRepository> logger)
    {
        _context = context;
        _logger = logger;
    }
    public async Task AddRangeAsync(IEnumerable<RequestInfoData> data)
    {
        //await _context.CreateDependsTable();

        //using var bulk = _context.GetBulkCopy();
        //var bulk = _context.

        //await bulk.InitAsync();
        //try
        //{
        //    await bulk.WriteToServerAsync(data);
        //}
        //catch (Exception ex) 
        //{
        //    _logger.LogError(ex.Message);
        //}

        try
        {
            await _context.AddRangeAsync(data);
            await _context.SaveChangesAsync();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex.Message);
        }
    }

    public async Task<ICollection<RequestInfoData>?> GetStatisticByUserIdAndDataRangeAsync(string userId, AnaliticsDataRangeModel dataRange)
    {
        return await _context.GetStatisticByUserIdAndDataRangeAsync(userId,  dataRange);
    }
}