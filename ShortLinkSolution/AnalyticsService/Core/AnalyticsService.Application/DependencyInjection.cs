using AnalyticsService.Application.DomaimServices;
using AnalyticsService.Domain.DomainServicesAbstractions;
using Microsoft.Extensions.DependencyInjection;

namespace AnalyticsService.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<IStatisticService, StatisticService>();
        return serviceCollection;
    }
}