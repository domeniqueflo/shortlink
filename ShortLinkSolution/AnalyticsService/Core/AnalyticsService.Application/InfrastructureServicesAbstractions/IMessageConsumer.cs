using ProjectLibrary.Realizations;

namespace AnalyticsService.Application.InfrastructureServicesAbstractions;

public interface IMessageConsumer
{
    public List<RequestInfoData> GetVisitedInformationMessages();
}