﻿using AnalyticsService.Domain.DomainServicesAbstractions;
using AnalyticsService.Domain.RepositoriesAbstraction;
using ProjectLibrary.Extensions;
using ProjectLibrary.Realizations;

namespace AnalyticsService.Application.DomaimServices
{
    public class StatisticService : IStatisticService
    {
        private readonly IAnalyticsRepository _repository;

        public StatisticService(IAnalyticsRepository repository)
        {
            _repository = repository;   
        }

        public async Task AddStatisticInfoAsync(IEnumerable<RequestInfoData> info)
        {
            //var data = RequestInfoDataClickHouseSerializer.Serialize(info);
            await _repository.AddRangeAsync(info);
        }

        public async Task<IEnumerable<RequestInfoData>?> GetLinkStatisticByUserIdAsync(string userId, AnaliticsDataRangeModel dataRange)
        {
            return await _repository.GetStatisticByUserIdAndDataRangeAsync(userId, dataRange);
        }
    }
}
