using ProjectLibrary.Realizations;
using System.Collections;

namespace AnalyticsService.Domain.RepositoriesAbstraction;

public interface IAnalyticsRepository
{
    public Task AddRangeAsync(IEnumerable<RequestInfoData> data);
    public Task<ICollection<RequestInfoData>?> GetStatisticByUserIdAndDataRangeAsync(string userId, AnaliticsDataRangeModel dataRange);

}