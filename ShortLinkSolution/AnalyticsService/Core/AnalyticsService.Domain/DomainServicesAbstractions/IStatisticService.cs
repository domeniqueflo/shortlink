﻿using ProjectLibrary.Realizations;

namespace AnalyticsService.Domain.DomainServicesAbstractions
{
    public interface IStatisticService
    {
        public Task AddStatisticInfoAsync(IEnumerable<RequestInfoData> info);
        public Task<IEnumerable<RequestInfoData>?> GetLinkStatisticByUserIdAsync(string userId, AnaliticsDataRangeModel dataRange);

    }
}
