﻿using ProjectLibrary.Interfaces;
using ProjectLibrary.Realizations;

namespace LinkService.Domain.DomainServiceAbstractions
{
    public interface ILinkService
    {
        Task<string> CreateNewShortLinkAsync(ILinkShortDataModel linkUserModel);

        Task<List<string>> GetUserShortLinks(string userId);

        Task<List<LinkFullDataModel>> GetAllUserLinkAsync(string userId);

    }
}
