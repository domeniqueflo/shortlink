﻿using ProjectLibrary.Interfaces;
using ProjectLibrary.Realizations;

namespace LinkService.Domain.RepositoriesAbstractions
{
    public interface IRepository
    {
        Task<bool> AddLinkAsync(ILinkShortDataModel linkUserModel, CancellationToken cancellationToken);
        Task<List<LinkFullDataModel>> GetAllUserLinksAsync(string email, CancellationToken cancellationToken);
        Task UpdateLinkAsync(ILinkShortDataModel linkUserModel, CancellationToken cancellationToken);
        Task DeleteLinkAsync(string shortLink, CancellationToken cancellationToken);
    }
}
