namespace LinkService.Domain.RepositoriesAbstractions;

public interface ILinkRepository
{
    public Task<string?> GetLinkAsync();
}