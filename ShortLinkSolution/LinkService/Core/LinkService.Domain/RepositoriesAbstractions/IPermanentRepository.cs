﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkService.Domain.RepositoriesAbstractions
{
    public interface IPermanentRepository : IRepository
    {
        Task<bool> CheckShortLinkName(string shortLinkName, CancellationToken cancellationToken);
        Task<List<string>> GetUserShortLinks(string userId, CancellationToken cancellationToken);
    }
}
