﻿using LinkService.Domain.DomainServiceAbstractions;
using Microsoft.Extensions.DependencyInjection;
using System.ComponentModel.Design;
using LinkService.Application.DomainServices;

namespace LinkService.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddScoped<ILinkService, DomainServices.LinkService>();
                
            return services;
        }
    }
}
