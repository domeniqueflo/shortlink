﻿using LinkService.Domain.DomainServiceAbstractions;
using LinkService.Domain.RepositoriesAbstractions;
using ProjectLibrary.Interfaces;
using ProjectLibrary.Realizations;


namespace LinkService.Application.DomainServices
{
    public class LinkService : ILinkService
    {
        private readonly IPremiumRepository _premiumRepository;
        private readonly IPermanentRepository _permanentRepository;
        private readonly ILinkRepository _linkRepository;
        private CancellationTokenSource _cancellationTokenSource;

        public LinkService(
            IPremiumRepository premiumRepository, 
            IPermanentRepository permanentRepository,
            ILinkRepository linkRepository)
        {
            _premiumRepository = premiumRepository;
            _permanentRepository = permanentRepository;
            _linkRepository = linkRepository;
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public async Task<string> CreateNewShortLinkAsync(ILinkShortDataModel linkUserModel)
        {
            //TODO: мы должны работать с заранее сгенерированными свободными именами и выбирать из них
            string shortLinkName = await _linkRepository.GetLinkAsync() ?? ""; 

            while (string.IsNullOrEmpty(shortLinkName))
            {
                shortLinkName = await _linkRepository.GetLinkAsync() ?? "";
            }

            linkUserModel.ShortLink = shortLinkName;

            if (await _permanentRepository.AddLinkAsync(linkUserModel, _cancellationTokenSource.Token))
            {
                if (!await _premiumRepository.AddLinkAsync(linkUserModel, _cancellationTokenSource.Token))
                {
                    return "PremiumDB_Save_Failed";
                }
            }
            else
            {
                return "PermanentDB_Save_Failed";
            }

            return shortLinkName;
        }

        public async Task<List<string>> GetUserShortLinks(string userId)
        {
            List<string> result = await _permanentRepository.GetUserShortLinks(userId, _cancellationTokenSource.Token);

            return result;
        }

        public async Task<List<LinkFullDataModel>> GetAllUserLinkAsync(string userId)
        {
            var result = await _permanentRepository.GetAllUserLinksAsync(userId, _cancellationTokenSource.Token);
            return result;
        }
    }
}

