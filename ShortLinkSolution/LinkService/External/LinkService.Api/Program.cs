using LinkService.Application;
using LinkService.Persistence;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using ProjectLibrary.Interfaces;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddApplication().AddPersistence(builder.Configuration);
builder.Configuration.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
builder.Services.AddSingleton<ILoggerManager>(provider =>
    new LoggerManager(provider.GetRequiredService<IConfiguration>()));

builder.Services.AddControllers();
builder.Host.UseSerilog();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddOpenIdConnect(options =>
    {
        options.ClientId = "spa";
        //options.Scope.Add("roles");
        options.Authority = "http://identity-service:8080";
        options.GetClaimsFromUserInfoEndpoint = true;
        options.SaveTokens = true;
        options.RequireHttpsMetadata = false;
    })
    .AddJwtBearer("Bearer", options =>
    {
        options.Authority = "http://identity-service:8080";
        options.Audience = "user-service";
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false
        };
        options.RequireHttpsMetadata = false;
    });

var app = builder.Build();

AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

try
{
    var logger = app.Services.GetRequiredService<ILoggerManager>();
    logger.LogInfo("Starting up");
    app.Run();
}
catch (Exception ex)
{
    var logger = app.Services.GetRequiredService<ILoggerManager>();
    logger.LogError($"Application start-up failed: {ex.Message}");
    throw;
}
finally
{
    Log.CloseAndFlush();
}