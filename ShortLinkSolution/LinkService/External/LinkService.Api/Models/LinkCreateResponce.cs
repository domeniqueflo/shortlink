﻿namespace LinkService.Api.Models
{
    public class LinkCreateResponce
    {
        public string WebLink { get; set; }
        public string MobileLink { get; set; }
    }
}
