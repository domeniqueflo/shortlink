﻿using ProjectLibrary.Interfaces;

namespace LinkService.Api.Models
{
    public class LinkShortDataModel : ILinkShortDataModel
    {
        public string UserId { get; set; }
        public string ShortLink { get; set; }
        public string WebLink { get; set; }
        public string MobileLink { get; set; }
    }
}
