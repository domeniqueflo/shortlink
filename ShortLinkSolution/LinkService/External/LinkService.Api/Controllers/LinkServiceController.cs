﻿using LinkService.Api.Models;
using LinkService.Domain.DomainServiceAbstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace LinkService.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class LinksController : ControllerBase
    {
        private readonly ILogger<LinksController> _logger;
        private readonly ILinkService _linkService;

        public LinksController(ILogger<LinksController> logger, ILinkService linkService)
        {
            _logger = logger;
            _linkService = linkService;
        }

        [AllowAnonymous]
        [HttpPost("Create")]
        public async Task<IActionResult> CreateShortLinkAsync([FromBody] LinkCreateResponce linkModel)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            //TODO: Get userID from claims
            var shortLinkModel = new LinkShortDataModel()
            {
                WebLink = linkModel.WebLink,
                MobileLink = linkModel.MobileLink,
                UserId = userId,
            };


            var result = await _linkService.CreateNewShortLinkAsync(shortLinkModel);
            
            return Ok(result);
        }

        [HttpGet("GetUserShortLinks")]
        public async Task<IActionResult> GetUserShortLinks()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var result = await _linkService.GetUserShortLinks(userId);

            return Ok(result);
        }

        [HttpGet("GetAllUserLink")]
        public async Task<IActionResult> GetAllUserLinkAsync()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var result = await _linkService.GetAllUserLinkAsync(userId);

            return Ok(result);
        }
    }
}