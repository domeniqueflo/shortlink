﻿using Microsoft.EntityFrameworkCore;
using LinkService.Domain.RepositoriesAbstractions;
using Microsoft.Extensions.DependencyInjection;
using LinkService.Persistence.Repositories;
using Microsoft.Extensions.Configuration;
using StackExchange.Redis;

namespace LinkService.Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<LinkDBContext>(
            options => options.UseNpgsql(Environment.GetEnvironmentVariable("DATABASE")));

            services.AddTransient<IPermanentRepository, PGLinkServiceRepository>();

            var redisConnectionString = Environment.GetEnvironmentVariable("redis")!;
            var keyDbConnectionString = Environment.GetEnvironmentVariable("KeyDbConnection")!;
            
            services.AddTransient<ILinkRepository, KeyDbLinkRepository>(provider =>
            {
                var multiplexer = ConnectionMultiplexer.Connect(keyDbConnectionString);
                var database = multiplexer.GetDatabase();
                return new KeyDbLinkRepository(database);
            });
            
            var redisMultiplexer = ConnectionMultiplexer.Connect(redisConnectionString);
            services.AddSingleton<IConnectionMultiplexer>(redisMultiplexer);
            services.AddTransient<IPremiumRepository, RedisLinkServiceRepository>();
          
            return services;
        }
    }
}
