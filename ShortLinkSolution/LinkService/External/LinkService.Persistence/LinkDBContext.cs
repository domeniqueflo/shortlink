﻿using LinkService.Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using ProjectLibrary.Realizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkService.Persistence
{
    public class LinkDBContext : DbContext
    {
        public LinkDBContext()
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }

        public DbSet<LinkFullDataModel> LinkTable { get; set; }

        public LinkDBContext(DbContextOptions<LinkDBContext> dbOptions) : base(dbOptions)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            Database.EnsureCreated();

            //try
            //{
            //    var databaseCreator = (Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator);
            //    databaseCreator.CreateTables();
            //}
            //catch
            //{
            //    //A SqlException will be thrown if tables already exist. So simply ignore it.
            //}
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(Environment.GetEnvironmentVariable("DATABASE"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<LinkFullDataModel>().HasIndex(i => i.ShortLink).IsUnique();
        }
    }
}
