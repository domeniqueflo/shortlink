using LinkService.Domain.RepositoriesAbstractions;
using Serilog;
using StackExchange.Redis;

namespace LinkService.Persistence.Repositories;

public class KeyDbLinkRepository : ILinkRepository
{
    private readonly IDatabase _database;
    private const string AvailableLinksKey = "AvailableLinks";
    public KeyDbLinkRepository(IDatabase database)
    {
        _database = database;
    }

    public async Task<string?> GetLinkAsync()
    {
        var link = await _database.ListRightPopAsync(AvailableLinksKey);

        if (link.IsNullOrEmpty)
        {
            Log.Fatal("No available links.");
            return null;
        }
        
        await _database.ListRemoveAsync(AvailableLinksKey, link);
        
        /*var db = _connectionMultiplexer.GetDatabase();
        var generatedLink = await db.StringGetAsync(AvailableLinksKey);
        // Drop our link from available links
        await db.ListRemoveAsync(AvailableLinksKey, generatedLink);*/
        return link.IsNullOrEmpty ? null : link.ToString();    
    }
}