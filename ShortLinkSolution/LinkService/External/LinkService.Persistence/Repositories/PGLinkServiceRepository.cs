﻿using LinkService.Domain.RepositoriesAbstractions;
using Microsoft.EntityFrameworkCore;
using ProjectLibrary.Interfaces;
using ProjectLibrary.Realizations;
using System.Threading.Tasks;

namespace LinkService.Persistence.Repositories
{
    public class PGLinkServiceRepository : IPermanentRepository
    {
        private readonly LinkDBContext _dbContext;
        public PGLinkServiceRepository(LinkDBContext dBContext)
        {
            _dbContext = dBContext;
        }

        public async Task<bool> CheckShortLinkName(string shortLinkName, CancellationToken cancellationToken)
        {
            if (await _dbContext.LinkTable.FirstOrDefaultAsync(el => el.ShortLink == shortLinkName) is null) { return true; }

            return false;
        }


        public async Task<bool> AddLinkAsync(ILinkShortDataModel linkUserModel, CancellationToken cancellationToken)
        {
            //TODO: add cancellation token logic
            try
            {
                var fullLink = new LinkFullDataModel(linkUserModel);
                await _dbContext.LinkTable.AddAsync(fullLink);
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<List<string>> GetUserShortLinks(string userId, CancellationToken cancellationToken)
        {
            return await _dbContext.LinkTable.Where(el => el.UserId == userId).Select(el => el.ShortLink).ToListAsync();
        }

        public Task DeleteLinkAsync(string shortLink, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<List<LinkFullDataModel>> GetAllUserLinksAsync(string userId, CancellationToken cancellationToken)
        {
            var result =  await _dbContext.LinkTable.Where(el => el.UserId == userId).ToListAsync();
            return result;
        }

        public Task UpdateLinkAsync(ILinkShortDataModel linkUserModel, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
