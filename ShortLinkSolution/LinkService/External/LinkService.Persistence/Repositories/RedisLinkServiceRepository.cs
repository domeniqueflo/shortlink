﻿using LinkService.Domain.RepositoriesAbstractions;
using ProjectLibrary.Interfaces;
using ProjectLibrary.Realizations;
using StackExchange.Redis;
using System.Text.Json;

namespace LinkService.Persistence.Repositories
{
    public class RedisLinkServiceRepository : IPremiumRepository
    {
        private readonly IDatabase _database;

        public RedisLinkServiceRepository(IConnectionMultiplexer redis)
        {
            _database = redis.GetDatabase();
        }

        public async Task<bool> AddLinkAsync(ILinkShortDataModel linkUserModel, CancellationToken cancellationToken)
        {
            //TODO: add cancellation token logic

            try
            {
                var linkJson = JsonSerializer.SerializeToUtf8Bytes(new LinkFullDataModel(linkUserModel));
                await _database.StringSetAsync(linkUserModel.ShortLink, linkJson);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public Task DeleteLinkAsync(string shortLink, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<List<LinkFullDataModel>> GetAllUserLinksAsync(string email, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task UpdateLinkAsync(ILinkShortDataModel linkUserModel, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
