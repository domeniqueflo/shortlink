﻿using identity_service.domain.Enitites;
using identity_service.IdentityConfig;
using identity_service.persistence.Database;
using Microsoft.AspNetCore.Identity;

namespace identity_service
{
    public static class DependecyInjection
    {
        public static IServiceCollection AddIdentityServerConfig(this IServiceCollection services)
        {
            services.AddIdentity<User, IdentityRole>()
                   .AddRoles<IdentityRole>()
                   .AddEntityFrameworkStores<UserDBContext>()
                   .AddDefaultTokenProviders();

            services.AddIdentityServer(options =>
            {
                options.IssuerUri = "http://identity-service:8080";
            })
               .AddInMemoryClients(Configurator.Clients)
               .AddInMemoryIdentityResources(Configurator.IdentityResources)
               .AddInMemoryApiResources(Configurator.ApiResources)
               .AddInMemoryApiScopes(Configurator.ApiScopes)
               .AddAspNetIdentity<User>()
               .AddProfileService<IdentityProfileService>()
               .AddDeveloperSigningCredential();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                });
            });

            return services;
        }
    }
}
