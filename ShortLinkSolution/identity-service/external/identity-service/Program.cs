using identity_service.persistence;
using Microsoft.AspNetCore.DataProtection;
using ProjectLibrary.Interfaces;
using ProjectLibrary.Realizations;
using Prometheus;
using Serilog;

namespace identity_service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            builder.Services.AddPersistence();
            builder.Host.UseSerilog();

            builder.Services.AddDataProtection()
               .UseEphemeralDataProtectionProvider();

            builder.Services.AddAuthorization();
            builder.Services.AddIdentityServerConfig();
            builder.Services.AddSingleton<ILoggerManager>(provider =>
                new LoggerManager(provider.GetRequiredService<IConfiguration>()));
            builder.Services.AddMonitoring(builder.Configuration, Environment.GetEnvironmentVariable("PROMETHEUS_PORT")!);

            var app = builder.Build();
            
            if (app.Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseIdentityServer();    
            app.UseAuthorization();
            app.UseMetricServer();
            app.UseHttpMetrics();
            
            try
            {
                var logger = app.Services.GetRequiredService<ILoggerManager>();
                logger.LogInfo("Starting up");
                app.Run();
            }
            catch (Exception ex)
            {
                var logger = app.Services.GetRequiredService<ILoggerManager>();
                logger.LogError($"Application start-up failed: {ex.Message}");
                throw;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}
