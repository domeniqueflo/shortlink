﻿using IdentityServer4;
using IdentityServer4.Models;

namespace identity_service.IdentityConfig
{
    public static class Configurator
    {
        public static IEnumerable<ApiResource> ApiResources =>
        [
             new ApiResource("user-service")
        ];

        public static IEnumerable<IdentityResource> IdentityResources =>
        [
            new IdentityResources.OpenId(),
            new IdentityResources.Profile()
            {
                UserClaims = new List<string>
                {
                    "name",
                    "email",
                    "role",
                }
            },
        ];

        public static IEnumerable<ApiScope> ApiScopes =>
        [
            new ApiScope("user-service", "US-S1"),
            new ApiScope("analytics-service", "AS-S1")
        ];

        public static IEnumerable<Client> Clients =>
            new List<Client>()
            {
                new Client
                {
                    ClientId = "spa",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    ClientSecrets =
                    {
                        new Secret("spa_secret".Sha256())
                    },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.ProtocolTypes.OpenIdConnect,
                        IdentityServerConstants.ProfileDataCallers.ClaimsProviderIdentityToken,
                        "user-service",
                        "analytics-service"
                    }
                }            
            };
    }
}
