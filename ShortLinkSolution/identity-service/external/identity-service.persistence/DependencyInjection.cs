﻿using identity_service.persistence.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace identity_service.persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection service)
        {
            service.AddDbContext<UserDBContext>(
                options => options.UseNpgsql(Environment.GetEnvironmentVariable("DATABASE1")));

            return service;
        }

    }
}
