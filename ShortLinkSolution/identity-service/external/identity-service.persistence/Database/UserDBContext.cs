﻿using identity_service.domain.Enitites;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace identity_service.persistence.Database
{
    public class UserDBContext : IdentityDbContext<User, IdentityRole, string>
    {
        public UserDBContext(DbContextOptions<UserDBContext> dbOptions) : base(dbOptions)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>(e => e.ToTable("users"));

            builder.Entity<IdentityRole>(entity => entity.ToTable(name: "roles"));
            builder.Entity<IdentityUserRole<string>>(entity =>
                entity.ToTable(name: "userRoles"));
            builder.Entity<IdentityUserClaim<string>>(entity =>
                entity.ToTable(name: "userClaims"));
            builder.Entity<IdentityUserLogin<string>>(entity =>
                entity.ToTable("userLogins").HasKey(l => new { l.LoginProvider, l.ProviderKey }));
            builder.Entity<IdentityUserToken<string>>(entity =>
                entity.ToTable("userTokens").HasKey(t => new { t.UserId, t.LoginProvider, t.Name }));
            builder.Entity<IdentityRoleClaim<string>>(entity =>
                entity.ToTable("roleClaims"));
        }
    }
}
