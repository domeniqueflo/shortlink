﻿using Microsoft.AspNetCore.Identity;

namespace identity_service.domain.Enitites
{
    public class User : IdentityUser
    {
        public int PrimeLinksCounter { get; set; }
    }
}
