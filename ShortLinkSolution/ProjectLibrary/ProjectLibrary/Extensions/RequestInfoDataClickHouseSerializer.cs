﻿using ProjectLibrary.Realizations;

namespace ProjectLibrary.Extensions
{
    public static class RequestInfoDataClickHouseSerializer
    {
        public static IEnumerable<object[]> Serialize(IEnumerable<RequestInfoData> data)
        {
            return data.Select(i => new object[]
            {
                 i.RequestDateTime,
                 i.ShortLink,
                 i.Language,
                 i.Country,
                 i.City,
                 i.BrowserInfo,
                 i.UserId,
                 i.IsMobile,
                 i.ReceivedLink ?? String.Empty,
            }).ToArray();
        }
    }
}
