using ProjectLibrary.Interfaces;

namespace ProjectLibrary.Realizations;

public sealed record RequestInfoData(
    string RequestDateTime,
    string ShortLink,
    string Language,
    string Country,
    string City,
    string BrowserInfo,
    string UserId,
    bool IsMobile,
    string? ReceivedLink) : IRequestInfoData;