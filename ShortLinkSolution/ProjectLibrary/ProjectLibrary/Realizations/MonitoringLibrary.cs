using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Prometheus;
using Serilog;
using Serilog.Sinks.Grafana.Loki;

namespace ProjectLibrary.Realizations;

public static class MonitoringSetup
{
    public static void AddMonitoring(this IServiceCollection services, IConfiguration configuration, string port)
    {
        ushort.TryParse(port, out var prometheusPort);
        
        Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(configuration)
            .Enrich.FromLogContext()
            .WriteTo.Console()
            .WriteTo.GrafanaLoki(configuration["Loki:Url"]!)
            .CreateLogger();

        services.AddLogging(loggingBuilder =>
            loggingBuilder.AddSerilog(dispose: true));
        
        var metricServer = new KestrelMetricServer(prometheusPort);
        metricServer.Start();
        services.AddSingleton<IMetricServer>(metricServer);
    }
}