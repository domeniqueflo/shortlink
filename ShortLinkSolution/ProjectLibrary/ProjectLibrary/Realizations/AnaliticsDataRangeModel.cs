﻿namespace ProjectLibrary.Realizations
{
    public sealed record AnaliticsDataRangeModel(DateTime DateFrom, DateTime DateTo);

}
