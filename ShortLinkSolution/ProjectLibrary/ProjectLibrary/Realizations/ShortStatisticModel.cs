﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLibrary.Realizations
{
    public class ShortStatisticModel
    {
        public string Date { get ; set; }
        public string ShortLink { get; set; }
        public int Clicks { get; set; }
        public string WebLink { get; set; }
        public string MobileLink { get; set; }
    }
}
