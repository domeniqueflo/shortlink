using System.Reflection;
using Microsoft.Extensions.Configuration;
using ProjectLibrary.Interfaces;
using Serilog;
using Serilog.Sinks.Grafana.Loki;

public class LoggerManager : ILoggerManager
{
    public LoggerManager(IConfiguration configuration)
    {
        var serviceName = Assembly.GetEntryAssembly()?.GetName().Name ?? "UnknownService";

        Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(configuration)
            .Enrich.WithProperty("ServiceName", serviceName)
            .Enrich.FromLogContext()
            .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] [{Service}] {Message:lj} {NewLine}{Exception}")
            .WriteTo.File("/var/log/log-.txt", rollingInterval: RollingInterval.Day, outputTemplate: "◨ {ServiceName} ◧ [⏲️{Timestamp:HH:mm:ss}] {Level:u3} 📃{Message:lj}{NewLine}{Exception}")
            .WriteTo.GrafanaLoki(configuration["Loki:Url"])
            .CreateLogger();
    }

    public void LogInfo(string message)
    {
        Log.Information(message);
    }

    public void LogWarn(string message)
    {
        Log.Warning(message);
    }

    public void LogDebug(string message)
    {
        Log.Debug(message);
    }

    public void LogError(string message)
    {
        Log.Error(message);
    }
}