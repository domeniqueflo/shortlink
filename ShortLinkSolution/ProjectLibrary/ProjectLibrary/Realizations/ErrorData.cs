using ProjectLibrary.Interfaces;

namespace ProjectLibrary.Realizations;

public sealed record ErrorData(long ErrorDateTime, string ShortLink) : IErrorData;