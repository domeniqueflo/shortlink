﻿using ProjectLibrary.Interfaces;

namespace ProjectLibrary.Realizations;

    public class LinkFullDataModel
{
    public int Id { get; set; }
    public string UserId { get; set; }
    public string ShortLink { get; set; }
    public string WebLink { get; set; }
    public string MobileLink { get; set; }
    public DateTime CreationDate { get; set; }
    public DateTime ExpirationDate { get; set; }
    public LinkStatus LinkStatusId { get; set; }
    public bool IsPremium { get; set; }

    public LinkFullDataModel() { }

    public LinkFullDataModel(ILinkShortDataModel linkShortDataModel)
    {
        ShortLink = linkShortDataModel.ShortLink;
        WebLink = linkShortDataModel.WebLink;
        MobileLink = linkShortDataModel.MobileLink;
        CreationDate = DateTime.Now;
        UserId = linkShortDataModel.UserId;
        //CreationDate = DateTime.SpecifyKind(CreationDate, DateTimeKind.Unspecified);
        ExpirationDate = CreationDate.AddDays(180);
        //ExpirationDate = DateTime.SpecifyKind(ExpirationDate, DateTimeKind.Unspecified);
        LinkStatusId = LinkStatus.Enabled;
        IsPremium = true;
    }
}

