﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLibrary.Interfaces;

    public interface ILinkShortDataModel
    {
        string UserId { get; set; }
        string ShortLink { get; set; }
        string WebLink { get; set; }
        string MobileLink { get; set; }
    }

