namespace ProjectLibrary.Interfaces;

public interface IRequestInfoData
{
        string RequestDateTime { get; init; }
        string ShortLink { get; init; }
        string Language { get; init; }
        string Country { get; init; }
        string City { get; init; }
        string BrowserInfo { get; init; }
        string UserId { get; init; }
        bool IsMobile { get; init; }
        string? ReceivedLink { get; init; }
}