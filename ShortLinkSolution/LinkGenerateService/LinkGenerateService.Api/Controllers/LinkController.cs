using LinkGenerateService.Domain.Entity;
using LinkGenerateService.Domain.RepositoriesAbstractions;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace LinkGenerateService.Api.Controllers;

[ApiController]
[Route("api")]
public class LinkController : Controller
{
    private readonly ILinkProviderService _linkProviderService;
    
    public LinkController(ILinkProviderService linkProviderService)
    {
        _linkProviderService = linkProviderService;
        
        Log.Warning("LinkController created.");
    }
    
    [HttpPost("generate-new-links")]
    public async Task<IActionResult> GenerateNewLinks() //[FromBody] int count == 1000)
    {
        var count = 10;
        try
        {
            await _linkProviderService.GenerateNewLinksAsync(count);
            return Ok("Links generated successfully.");
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Failed to generate links: {ex.Message}");
        }
    }
}