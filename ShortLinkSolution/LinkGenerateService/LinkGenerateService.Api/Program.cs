using LinkGenerateService.Application;
using LinkGenerateService.Application.DomainServices;
using LinkGenerateService.Infrastructure;
using LinkGenerateService.Persistence;
using ProjectLibrary.Interfaces;
using Serilog;

//var builder = WebApplication.CreateBuilder(args);

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseSerilog((context, config) => {
    config.ReadFrom.Configuration(context.Configuration);
    config.WriteTo.Console();
});


builder.Host.UseSerilog();

// Add services to the container.
builder.Configuration.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
builder.Configuration.AddEnvironmentVariables();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddApplication();
builder.Services.AddPersistence(builder.Configuration);
builder.Services.AddInfrastructure();
builder.Services.AddSingleton<ILoggerManager>(provider =>
    new LoggerManager(provider.GetRequiredService<IConfiguration>()));
builder.Services.AddHostedService<LinkGenerationBackgroundService>();
//builder.WebHost.UseUrls("http://*:80", "https://*:443");


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseDeveloperExceptionPage();
}

//app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
Console.WriteLine("Link   " );

try
{
    var logger = app.Services.GetRequiredService<ILoggerManager>();
    logger.LogInfo("Starting up");
    app.Run();
}
catch (Exception ex)
{
    var logger = app.Services.GetRequiredService<ILoggerManager>();
    logger.LogError($"Application start-up failed: {ex.Message}");
    throw;
}
finally
{
    Log.CloseAndFlush();
}