using LinkGenerateService.Domain.Entity;
using LinkGenerateService.Domain.RepositoriesAbstractions;
using Serilog;
using StackExchange.Redis;

namespace LinkGenerateService.Persistence.Repositories;

public class KeyDbLinkRepository : ILinkRepository
{
    private readonly IConnectionMultiplexer _connectionMultiplexer;
    private const string AvailableLinksKey = "AvailableLinks";
    private const string LastGeneratedLinkKey = "LastGeneratedLink";
    public KeyDbLinkRepository(IConnectionMultiplexer keyDb)
    {
        _connectionMultiplexer = keyDb;
    }

    public async Task SaveLinkListAsync(List<ShortLink> links)
    {
        var db = _connectionMultiplexer.GetDatabase();
        var linksKeyDb = links.Select(link => link.ShortLinkValue).Select(dummy => (RedisValue)dummy).ToArray();

        try
        {
            await _connectionMultiplexer.GetDatabase().ListLeftPushAsync(AvailableLinksKey, linksKeyDb);
        }
        catch (Exception e)
        {
            Log.Error("Error on saving links: " + e);
        }
    }

    public async Task<string?> GetLastLinkAsync()
    {
        var db = _connectionMultiplexer.GetDatabase();
        var lastGeneratedLink = await db.StringGetAsync(LastGeneratedLinkKey);
        return lastGeneratedLink.IsNullOrEmpty ? null : lastGeneratedLink.ToString();    }

    public async Task SetLastLinkAsync(string shortLink)
    {
        await _connectionMultiplexer.GetDatabase().StringSetAsync(LastGeneratedLinkKey, shortLink);
    }

    public async Task<int> GetAvailableLinksCountAsync()
    {
        var db = _connectionMultiplexer.GetDatabase();
        var count = await db.ListLengthAsync(AvailableLinksKey);
        return (int)count;
    }
}