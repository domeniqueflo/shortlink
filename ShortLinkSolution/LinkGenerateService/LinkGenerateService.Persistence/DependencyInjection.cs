﻿using LinkGenerateService.Domain.RepositoriesAbstractions;
using LinkGenerateService.Persistence.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace LinkGenerateService.Persistence;

public static class DependencyInjection
{
    public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
    {
        var fff = $"{Environment.GetEnvironmentVariable("KeyDbConnection")},abortConnect=false";
        var multiplexer = ConnectionMultiplexer.Connect($"{Environment.GetEnvironmentVariable("KeyDbConnection")},abortConnect=false");
        services.AddSingleton<IConnectionMultiplexer>(multiplexer);
        services.AddTransient<ILinkRepository, KeyDbLinkRepository>();
        
        return services;
    }
}