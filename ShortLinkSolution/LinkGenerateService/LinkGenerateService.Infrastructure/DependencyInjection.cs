﻿using LinkGenerateService.Domain.RepositoriesAbstractions;
using LinkGenerateService.Infrastructure.InfrastructureServices;
using Microsoft.Extensions.DependencyInjection;

namespace LinkGenerateService.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services)
    {
        services.AddTransient<ILinkProviderService, LinkProviderService>();
        return services;
    }
}