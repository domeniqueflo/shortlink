using LinkGenerateService.Domain.Entity;
using LinkGenerateService.Domain.RepositoriesAbstractions;

namespace LinkGenerateService.Infrastructure.InfrastructureServices;

public class LinkProviderService : ILinkProviderService
{
    private readonly ILinkRepository _linkRepository;
    private static bool _isGenerating;


    public LinkProviderService(ILinkRepository linkRepository)
    {
        _linkRepository = linkRepository;
    }

    public async Task<string?> GetLastLinkAsync()
    {
        return await _linkRepository.GetLastLinkAsync();
    }

    public async Task<int> GetLinksCountAsync()
    {
        return await _linkRepository.GetAvailableLinksCountAsync();
    }

    public async Task SetLastLinkAsync(string shortLink)
    {
        await _linkRepository.SetLastLinkAsync(shortLink);
    }

    public async Task GenerateNewLinksAsync(int count)
    {
        if (_isGenerating)
        {
            return;
        }
        
        _isGenerating = true;

        try
        {
            var lastLink = await GetLastLinkAsync() ?? "999";
            var newLinks = ShortLinkGenerator.GenerateSequence(lastLink, count);
            await _linkRepository.SaveLinkListAsync(newLinks);
            await SetLastLinkAsync(newLinks.Last().ShortLinkValue);
        }
        finally
        {
            _isGenerating = false;
        }
    }
}