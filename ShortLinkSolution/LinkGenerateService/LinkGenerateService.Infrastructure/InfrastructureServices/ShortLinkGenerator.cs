using LinkGenerateService.Domain.Entity;

namespace LinkGenerateService.Infrastructure.InfrastructureServices;

public static class ShortLinkGenerator
{
    private const string CharOptions = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public static List<ShortLink> GenerateSequence(string baseString, int count)
    {
        var results = new List<string>();
        int currentIndex = GetCurrentIndex(baseString);

        for (int i = 0; i < count; i++)
        {
            currentIndex++;
            string newString = GenerateNextString(baseString.Length, currentIndex);
            results.Add(newString);
        }

        return results.Select(currentLink => new ShortLink() { ShortLinkValue = currentLink, IsOccupied = false }).ToList();
    }

    private static int GetCurrentIndex(string baseString)
    {
        int index = 0;
        int multiplier = 1;

        for (int i = baseString.Length - 1; i >= 0; i--)
        {
            index += CharOptions.IndexOf(baseString[i]) * multiplier;
            multiplier *= CharOptions.Length;
        }

        return index;
    }

    private static string GenerateNextString(int length, int index)
    {
        var result = new List<char>();

        while (index >= CharOptions.Length)
        {
            result.Insert(0, CharOptions[index % CharOptions.Length]);
            index = index / CharOptions.Length - 1;
        }

        result.Insert(0, CharOptions[index]);

        while (result.Count < length)
        {
            result.Insert(0, CharOptions[0]);
        }

        return new string(result.ToArray());
    }
}