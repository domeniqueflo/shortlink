namespace LinkGenerateService.Domain.Entity;

public class ShortLink
{
    public string ShortLinkValue { get; set; }
    public bool IsOccupied { get; set; }
}