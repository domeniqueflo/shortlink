namespace LinkGenerateService.Domain.Entity;

public class ReleaseLinkRequest
{
    public string ShortLink { get; set; }
}