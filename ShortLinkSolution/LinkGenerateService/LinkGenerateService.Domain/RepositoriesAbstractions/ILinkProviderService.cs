using LinkGenerateService.Domain.Entity;

namespace LinkGenerateService.Domain.RepositoriesAbstractions;

public interface ILinkProviderService
{
    Task<string?> GetLastLinkAsync();
    Task SetLastLinkAsync(string shortLink);
    Task GenerateNewLinksAsync(int count);
    public Task<int> GetLinksCountAsync();
}