using LinkGenerateService.Domain.Entity;

namespace LinkGenerateService.Domain.RepositoriesAbstractions;

public interface ILinkRepository
{
    Task SetLastLinkAsync(string shortLink);
    Task<int> GetAvailableLinksCountAsync();
    public Task SaveLinkListAsync(List<ShortLink> links);
    public Task<string?> GetLastLinkAsync();
}