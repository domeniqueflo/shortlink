﻿using LinkGenerateService.Domain.RepositoriesAbstractions;
using LinkGenerateService.Infrastructure.InfrastructureServices;
using Microsoft.Extensions.DependencyInjection;

namespace LinkGenerateService.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection services)
    {
        services.AddTransient<ILinkProviderService, LinkProviderService>();
        return services;
    }
}