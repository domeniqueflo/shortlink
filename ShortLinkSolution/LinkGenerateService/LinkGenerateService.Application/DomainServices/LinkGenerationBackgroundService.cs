using LinkGenerateService.Domain.RepositoriesAbstractions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace LinkGenerateService.Application.DomainServices;

public class LinkGenerationBackgroundService : BackgroundService
{
    private readonly IServiceProvider _serviceProvider;
    private const int Threshold = 10000; 
    private const int GenerationBatchSize = 1000;

    public LinkGenerationBackgroundService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await CheckAndGenerateLinksAsync();
            await Task.Delay(TimeSpan.FromMinutes(1), stoppingToken);
        }
    }

    private async Task CheckAndGenerateLinksAsync()
    { 
        using var scope = _serviceProvider.CreateScope();
        var linkProviderService = scope.ServiceProvider.GetRequiredService<ILinkProviderService>();

        var availableLinksCount = await linkProviderService.GetLinksCountAsync();
        Log.Information($"Links count = {availableLinksCount}");


        if (availableLinksCount < Threshold)
        {
            Log.Information($"Available links below threshold ({availableLinksCount}/{Threshold}). Generating new links.");
            await linkProviderService.GenerateNewLinksAsync(GenerationBatchSize);
            Log.Information("Link generation completed.");
        }
        else
        {
            Log.Information($"Available links count is sufficient ({availableLinksCount}/{Threshold}).");
        }
    }
}
