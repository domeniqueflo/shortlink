﻿namespace UserService.Domain.RepositoryAbstractions
{
    public interface IUserRepository
    {
        Task<string> AddUserToDBAsync(User user);
        Task<User?> GetUserByEmailAsync(string email);
        Task AddUserPersonalData(PersonalData personalData);
        Task<PersonalData?> GetUserPersonalData(string userId);
    }
}
