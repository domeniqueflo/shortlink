﻿namespace UserService.Domain.DomainServiceAbstractions
{
    public interface IUserService
    {
        Task<UserServiceBaseResponse> AddUserToDBAsync(string email, string password);
        Task<string?> AuthorizeUserAsync(User user);
        Task<UserServiceBaseResponse> ChangePasswordAsync(string userId, string oldPassword, string newPassword);
        Task<UserServiceBaseResponse> ChangeEmailAsync(string userId, string newEmail);
        Task<UserServiceBaseResponse> ChangePersonalInfo(string userId, PersonalData personalInfo);
        Task<PersonalData> GetPersonalInfo(string userId);
    }
}
