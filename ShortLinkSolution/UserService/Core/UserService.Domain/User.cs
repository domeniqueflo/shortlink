﻿using Microsoft.AspNetCore.Identity;

namespace UserService.Domain
{
    public class User : IdentityUser
    {
        public int PrimeLinksCounter { get; set; }
        public PersonalData PersonalInfo { get; set; }

        public User()
        {
            PersonalInfo = new PersonalData();
        }
    }
}
