﻿using Microsoft.Extensions.DependencyInjection;
using UserService.Domain.DomainServiceAbstractions;

namespace UserService.Application
{
    public static class DependensyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection service)
        {
            service.AddScoped<IUserService, DomainServices.UserService>();
            return service;
        }
    }
}
