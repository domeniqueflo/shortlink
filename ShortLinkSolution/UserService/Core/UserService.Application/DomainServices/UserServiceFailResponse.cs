﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Domain.DomainServiceAbstractions;

namespace UserService.Application.DomainServices
{
    public class UserServiceFailResponse : UserServiceBaseResponse
    {
        public List<string> Errors { get; set; }

        public UserServiceFailResponse() : base() 
        { 
            Errors = new List<string>();
        }
    }
}
