﻿using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using UserService.Domain;
using UserService.Domain.DomainServiceAbstractions;
using UserService.Domain.RepositoryAbstractions;

namespace UserService.Application.DomainServices
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly UserManager<User> _userManager;

        public UserService(IUserRepository userRepository, UserManager<User> userManager)
        {
            _userRepository = userRepository;
            _userManager = userManager;
        }

        public async Task<UserServiceBaseResponse> AddUserToDBAsync(string email, string password)
        {
            //if (_userRepository.GetUserByEmailAsync(email!) is not null) { return false; }

            //await _userRepository.AddUserToDBAsync(user);

            try
            {
                User newUser = new User() { UserName = email, Email = email };
                UserServiceBaseResponse result = new UserServiceBaseResponse() { Succeeded = true };
                result = CheckUserManagerResponse(await _userManager.CreateAsync(newUser, password)) ?? result;
                if (result is UserServiceFailResponse) return result;

                result = CheckUserManagerResponse(await _userManager.AddToRoleAsync(newUser, "common")) ?? result;
                if (result is UserServiceFailResponse) return result;

                var claims = new List<Claim>
            {
                new Claim("name", email),
                new Claim("email", email)
            };
            
            result = CheckUserManagerResponse(await _userManager.AddClaimsAsync(newUser, claims)) ?? result;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private UserServiceBaseResponse? CheckUserManagerResponse(IdentityResult response)
        {
            if (response.Succeeded) { return null; }

            UserServiceFailResponse result = new UserServiceFailResponse();
            foreach (var item in response.Errors)
            {
                result.Errors.Add(item.Description);
            }

            return result;
        }



        public async Task<string?> AuthorizeUserAsync(User user)
        {
            var storedUser = await _userRepository.GetUserByEmailAsync(user.Email!);

            if (storedUser is null) { return null; }

            if (user.PasswordHash != storedUser.PasswordHash) { return null; }

            return user.Email;
        }


        public async Task<UserServiceBaseResponse> ChangePasswordAsync(string userId, string oldPassword, string newPassword)
        {
            try
            {
                UserServiceBaseResponse result = new UserServiceBaseResponse() { Succeeded = true };

                User user = await _userManager.FindByIdAsync(userId);

                result = CheckUserManagerResponse(await _userManager.ChangePasswordAsync(user, oldPassword, newPassword)) ?? result;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<UserServiceBaseResponse> ChangeEmailAsync(string userId, string newEmail)
        {
            try
            {
                UserServiceBaseResponse result = new UserServiceBaseResponse() { Succeeded = true };

                User user = await _userManager.FindByIdAsync(userId);
                var confirmationToken = await _userManager.GenerateChangeEmailTokenAsync(user, newEmail);

                result = CheckUserManagerResponse(await _userManager.ChangeEmailAsync(user, newEmail, confirmationToken)) ?? result;

                if (result.Succeeded)
                {
                    result = CheckUserManagerResponse(await _userManager.SetUserNameAsync(user, newEmail)) ?? result;
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<UserServiceBaseResponse> ChangePersonalInfo(string userId, PersonalData personalInfo)
        {
            try
            {
                User user = await _userManager.FindByIdAsync(userId);
                UserServiceBaseResponse result = new UserServiceBaseResponse() { Succeeded = true };
                user = await _userRepository.GetUserByEmailAsync(user.Email);
               // var pi = await _userRepository.GetUserPersonalData(userId);

                user.PersonalInfo.FirstName = personalInfo.FirstName;
                user.PersonalInfo.SecondName = personalInfo.SecondName;
                user.PersonalInfo.LastName = personalInfo.LastName;
                user.PersonalInfo.UserId = user.Id;

                await _userRepository.AddUserPersonalData(user.PersonalInfo);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<PersonalData> GetPersonalInfo(string userId)
        {
            return await _userRepository.GetUserPersonalData(userId);
        }
    }
}
