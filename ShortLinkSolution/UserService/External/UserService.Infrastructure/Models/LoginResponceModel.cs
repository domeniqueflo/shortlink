﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserService.Infrastructure.Models
{
    public class LoginResponceModel
    {
        public string? ErrorMessage { get; set; }
        public string TokenResponce { get; set; }
    }
}
