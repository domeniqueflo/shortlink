﻿using System.Text.Json.Serialization;
using UserService.Infrastructure.Abstractions;

namespace UserService.Infrastructure.Models
{
    public class TokenSuccessResponce : ITokenResultResponce
    {
        [JsonPropertyName("access_token")]
        public string AccessToken { get; set; }
        [JsonPropertyName("expires_in")]
        public int ExpiresIn { get; set; }
        [JsonPropertyName("token_type")]
        public string TokenType { get; set; }
        [JsonPropertyName("refresh_token")]
        public string RefreshToken { get; set; }
    }
}
