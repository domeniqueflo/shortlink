﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace UserService.Infrastructure.Models
{
    //Registration info
    public class SignUpModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [JsonPropertyName("confirm_password")]
        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }   
    }
}
