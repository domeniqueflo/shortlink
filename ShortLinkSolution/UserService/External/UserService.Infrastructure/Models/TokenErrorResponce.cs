﻿using System.Text.Json.Serialization;
using UserService.Infrastructure.Abstractions;

namespace UserService.Infrastructure.Models
{
    public class TokenErrorResponce : ITokenResultResponce
    {
        [JsonPropertyName("error")]
        public string Error { get; set; }
        [JsonPropertyName("error_description")]
        public string ErrorDescription { get; set; }
    }
}
