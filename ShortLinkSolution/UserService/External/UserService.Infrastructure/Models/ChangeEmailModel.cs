﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserService.Infrastructure.Models
{
    public class ChangeEmailModel
    {
        public string NewEmail { get; set; }
    }
}
