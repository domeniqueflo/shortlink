﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using UserService.Domain;
using UserService.Infrastructure.Models;

namespace UserService.Infrastructure.Abstractions
{
    public interface ILinkServiceUserManager
    {
        public Task<User> GetUserByEmailAsync(string email);
        public Task<IdentityResult> CreateUserAsync(User user, string password);
        public Task<IActionResult> GetAuthResultAsync(SignInModel signInModel);
        public Task<IdentityResult> ChangePasswordAsync(User user, string oldPassword, string newPassword);
        public Task<IdentityResult> ResetPasswordAsync(User user, string accessToken, string newPassword);
    }
}
