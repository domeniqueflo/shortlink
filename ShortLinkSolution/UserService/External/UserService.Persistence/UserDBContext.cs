﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserService.Domain;

namespace UserService.Persistence
{
    public class UserDBContext : IdentityDbContext<User>
    {
        public DbSet<PersonalData> PersonalDatas { get; set; }

        public UserDBContext()
        {

        }

        public UserDBContext(DbContextOptions<UserDBContext> dbOptions) : base(dbOptions)
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(Environment.GetEnvironmentVariable("DATABASE"));
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>(e => e.ToTable("users"));
            builder.Entity<IdentityRole>(entity => entity.ToTable(name: "roles"));
            builder.Entity<IdentityUserRole<string>>(entity =>
                entity.ToTable(name: "userRoles"));
            builder.Entity<IdentityUserClaim<string>>(entity =>
                entity.ToTable(name: "userClaims"));
            builder.Entity<IdentityUserLogin<string>>(entity =>
                entity.ToTable("userLogins").HasKey(l => new { l.LoginProvider, l.ProviderKey }));
            builder.Entity<IdentityUserToken<string>>(entity =>
                entity.ToTable("userTokens").HasKey(t => new { t.UserId, t.LoginProvider, t.Name }));
            builder.Entity<IdentityRoleClaim<string>>(entity =>
                entity.ToTable("roleClaims"));
            builder.Entity<PersonalData>(e => e.ToTable("personaldata"));

            builder.Entity<User>()
               .HasOne(e => e.PersonalInfo)
               .WithOne(e => e.UserData)
               .HasForeignKey<PersonalData>(e => e.UserId);
        }


    }
}
