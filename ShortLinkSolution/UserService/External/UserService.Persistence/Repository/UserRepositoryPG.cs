﻿using Microsoft.EntityFrameworkCore;
using UserService.Domain;
using UserService.Domain.RepositoryAbstractions;

namespace UserService.Persistence.Repository
{
    public class UserRepositoryPG : IUserRepository
    {
        private readonly UserDBContext _dbContext;
        public UserRepositoryPG(UserDBContext dBContext)
        {
            _dbContext = dBContext;
        }

        public async Task<string> AddUserToDBAsync(User user)
        {
            await _dbContext.Users.AddAsync(user);
            _dbContext.SaveChanges();
            return user.Id;
        }

        public Task<User?> GetUserByEmailAsync(string email)
        {
            return _dbContext.Users.Include(el => el.PersonalInfo).FirstOrDefaultAsync(el => el.Email == email);
        }

        public async Task AddUserPersonalData(PersonalData personalData)
        {
            try
            {
                if (_dbContext.PersonalDatas.FirstOrDefault(el => el.Id == personalData.Id) is null)
                {
                    _dbContext.PersonalDatas.Add(personalData);
                }
                else
                {
                    _dbContext.PersonalDatas.Update(personalData);
                }
                _dbContext.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public async Task<PersonalData?> GetUserPersonalData(string userId)
        {
            return await _dbContext.PersonalDatas.FirstOrDefaultAsync(el => el.UserId == userId);
        }
    }
}
