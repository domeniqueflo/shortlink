﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using UserService.Domain.RepositoryAbstractions;
using UserService.Persistence.Repository;

namespace UserService.Persistence
{
    public static class DependensyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection service)
        {
            service.AddDbContext<UserDBContext>(
                options => options.UseNpgsql(Environment.GetEnvironmentVariable("DATABASE")));

            service.AddTransient<IUserRepository, UserRepositoryPG>();
            return service;
        }
    }
}
