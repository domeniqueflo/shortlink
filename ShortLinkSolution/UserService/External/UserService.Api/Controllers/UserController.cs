using IdentityModel.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using UserService.Application.DomainServices;
using UserService.Domain;
using UserService.Domain.DomainServiceAbstractions;
using UserService.Infrastructure.Abstractions;
using UserService.Infrastructure.Models;

namespace UserService.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;
        private readonly IHttpClientFactory _httpClientFactory;

        public UserController(ILogger<UserController> logger, IUserService userService, IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _userService = userService;
            _httpClientFactory = httpClientFactory;
        }

        [Authorize(Roles = "common")]       
        [HttpGet("claim")]
        public IActionResult TestGet()
        {
            var user = User;
            var result = user.Claims.FirstOrDefault(c => c.Type == "name");
            //return new JsonResult(from c in User.Claims select new { c.Type, c.Value });
            return new JsonResult(new { result.Type, result.Value });
        }

        [AllowAnonymous]
        [HttpPost("SignIn")]
        public async Task<IActionResult> SignInAsync([FromBody] SignInModel signInModel)
        {
            var client = _httpClientFactory.CreateClient();

            var disco = await client.GetDiscoveryDocumentAsync(new DiscoveryDocumentRequest
            {
                Address = "http://identity-service:8080",
                Policy =
                {
                    RequireHttps = false
                }
            });

            if (disco.IsError)
            {
                return BadRequest(disco.Error);
            }

            var tokenResponse = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "spa",
                ClientSecret = "spa_secret",
                UserName = signInModel.Email,
                Password = signInModel.Password,
            });

            ITokenResultResponce responce;
            IActionResult result;

            if (tokenResponse.IsError)
            {
                responce = new TokenErrorResponce()
                {
                    Error = tokenResponse.Error,
                    ErrorDescription = tokenResponse.ErrorDescription
                };
                result =  BadRequest(responce);
            }
            else
            {
                responce = new TokenSuccessResponce()
                {
                    AccessToken = tokenResponse.AccessToken,
                    ExpiresIn = tokenResponse.ExpiresIn,
                    RefreshToken = tokenResponse.RefreshToken,
                    TokenType = tokenResponse.TokenType
                };
                result = Ok(responce);
            }

            return result;
            
        }

        [AllowAnonymous]
        [HttpPost("SignUp")]
        public async Task<IActionResult> SignUpAsync([FromBody] SignUpModel signUpModel)
        {
            var result = await _userService.AddUserToDBAsync(signUpModel.Email, signUpModel.Password);

            if (result is UserServiceFailResponse failResponse)
            {
                return BadRequest(failResponse.Errors);
            }

            return Ok();
        }

        [Authorize(Roles = "common")]
        [HttpPost("ChangePassword")]
        public async Task<IActionResult> ChangePasswordAsync([FromBody] ChangePasswordModel changePasswordModel)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var result = await _userService.ChangePasswordAsync(userId, changePasswordModel.OldPassword, changePasswordModel.NewPassword);

            return GetResponse(result);
        }

        [Authorize(Roles = "common")]
        [HttpPost("ChangeEmail")]
        public async Task<IActionResult> ChangeEmailAsync([FromBody] ChangeEmailModel changeEmailModel)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var result = await _userService.ChangeEmailAsync(userId, changeEmailModel.NewEmail);

            return GetResponse(result);
        }

        [Authorize(Roles = "common")]
        [HttpPost("ChangePersonalInfo")]
        public async Task<IActionResult> ChangePersonalInfo([FromBody] PersonalData changePesonalInfoModel)
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var result = await _userService.ChangePersonalInfo(userId, changePesonalInfoModel);

           return GetResponse(result);
        }

        [Authorize(Roles = "common")]
        [HttpGet("GetPersonalInfo")]
        public async Task<IActionResult> GetPersonalInfo()
        {
            string userId = User.FindFirst(ClaimTypes.NameIdentifier)!.Value;

            var result = await _userService.GetPersonalInfo(userId);

            return Ok(result);
        }

        private IActionResult GetResponse(UserServiceBaseResponse response)
        {
            if (response is UserServiceFailResponse failResponse)
            {
                return BadRequest(failResponse.Errors);
            }

            return Ok();
        }
    }
}
