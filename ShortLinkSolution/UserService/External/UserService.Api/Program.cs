using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using UserService.Application;
using UserService.Domain;
using UserService.Persistence;
using ProjectLibrary.Interfaces;
using ProjectLibrary.Realizations;
using Prometheus;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddApplication().AddPersistence();
builder.Services.AddIdentity<User, IdentityRole>().AddRoles<IdentityRole>().AddEntityFrameworkStores<UserDBContext>().AddDefaultTokenProviders();
builder.Services.AddHttpClient();
builder.Host.UseSerilog();
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddOpenIdConnect(options =>
    {
        options.ClientId = "spa";
        //options.Scope.Add("roles");
        options.Authority = "http://identity-service:8080";
        options.GetClaimsFromUserInfoEndpoint = true;
        options.SaveTokens = true;
        options.RequireHttpsMetadata = false;
    })
    .AddJwtBearer("Bearer", options =>
    {
        options.Authority = "http://identity-service:8080";
        options.Audience = "user-service";
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false
        };
        options.RequireHttpsMetadata = false;
    });

builder.Services.AddCors(options => 
{
    options.AddPolicy("all", builder => 
    {
        builder
            .WithOrigins("*")
            .AllowAnyHeader()
            .AllowAnyMethod();
    });
});

builder.Services.AddAuthorization();
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<ILoggerManager>(provider =>
    new LoggerManager(provider.GetRequiredService<IConfiguration>()));
builder.Services.AddMonitoring(builder.Configuration, Environment.GetEnvironmentVariable("PROMETHEUS_PORT")!);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseDeveloperExceptionPage();
}

using (var scope = app.Services.CreateScope())
{
    var manager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
    var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();

    try
    {
        await manager.CreateAsync(new IdentityRole("common"));
    }
    catch (Exception ex)
    {
        logger.LogError(ex.ToString());
    }
}
app.UseAuthentication();
app.UseAuthorization();
app.UseMetricServer();
app.UseHttpMetrics();
app.UseCors("all");
app.MapControllers();

try
{
    var logger = app.Services.GetRequiredService<ILoggerManager>();
    logger.LogInfo("Starting up");
    app.Run();
}
catch (Exception ex)
{
    var logger = app.Services.GetRequiredService<ILoggerManager>();
    logger.LogError($"Application start-up failed: {ex.Message}");
    throw;
}
finally
{
    Log.CloseAndFlush();
}
